
package com.gudugudi.gudugudiinanandroidtrial;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SensorStat {
  
    @SerializedName("Shoolman")
    @Expose
    private Shoolman shoolman;

    @SerializedName("doorStatus")
    @Expose
    private List<Object> doorStatus = null;
    @SerializedName("hour")
    @Expose
    private List<Integer> hour = null;
    @SerializedName("minute")
    @Expose
    private List<Integer> minute = null;
    @SerializedName("month")
    @Expose
    private List<Integer> month = null;
    @SerializedName("seconds")
    @Expose
    private List<Integer> seconds = null;
    @SerializedName("terminalStatus")
    @Expose
    private List<Object> terminalStatus = null;
    @SerializedName("voltageLevel")
    @Expose
    private List<Double> voltageLevel = null;
    @SerializedName("year")
    @Expose
    private List<Integer> year = null;

    public Shoolman getShoolman() {
        return shoolman;
    }

    public void setShoolman(Shoolman shoolman) {
        this.shoolman = shoolman;
    }

    public List<Object> getDoorStatus() {
        return doorStatus;
    }

    public void setDoorStatus(List<Object> doorStatus) {
        this.doorStatus = doorStatus;
    }

    public List<Integer> getHour() {
        return hour;
    }

    public void setHour(List<Integer> hour) {
        this.hour = hour;
    }

    public List<Integer> getMinute() {
        return minute;
    }

    public void setMinute(List<Integer> minute) {
        this.minute = minute;
    }

    public List<Integer> getMonth() {
        return month;
    }

    public void setMonth(List<Integer> month) {
        this.month = month;
    }

    public List<Integer> getSeconds() {
        return seconds;
    }

    public void setSeconds(List<Integer> seconds) {
        this.seconds = seconds;
    }

    public List<Object> getTerminalStatus() {
        return terminalStatus;
    }

    public void setTerminalStatus(List<Object> terminalStatus) {
        this.terminalStatus = terminalStatus;
    }

    public List<Double> getVoltageLevel() {
        return voltageLevel;
    }

    public void setVoltageLevel(List<Double> voltageLevel) {
        this.voltageLevel = voltageLevel;
    }

    public List<Integer> getYear() {
        return year;
    }

    public void setYear(List<Integer> year) {
        this.year = year;
    }

}
