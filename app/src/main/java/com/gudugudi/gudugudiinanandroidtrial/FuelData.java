package com.gudugudi.gudugudiinanandroidtrial;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
public class FuelData  extends AsyncTask<Void,Void,Void> {

  String data="";

  int allSets = 0;
  public JSONArray dateJsObjecth;
  public JSONArray hrJsObjecth;
  public JSONArray latiJsObjecth;
  public JSONArray longiJsObjecth;
  public JSONArray minJsObjecth;
  public JSONArray mnJsObjecth;
  public JSONArray secJsObjecth;
  public JSONArray spJsObjecth;
  public JSONArray yrJsObjecth;

  public JSONArray terJsObject;
  public JSONArray doorStJsObject;
  public JSONArray voltJsObject;
  public JSONArray hrJsObject;
  public JSONArray minJsObject;
  public JSONArray mnJsObject;
  public JSONArray secJsObject;
  public JSONArray yrJsObject;
  @Override
  protected void onProgressUpdate(Void... values) {
    super.onProgressUpdate(values);
  }
  @Override
  protected Void doInBackground(Void... voids) {
    try {
//sftp://harish_shoully@www.gudugudi.com/var/www/html/tracksensors/0351510090112551-logs/0351510090112551-28-10-18.json
//sftp://harish_shoully@www.gudugudi.com/var/www/html/tracksensors/0351510090112551-logs/0351510090112551.json
      URL url=new URL("https://www.gudugudi.com/tracksensors/0351510090112551-logs/0351510090112551-12-10-18.json");

      URL url1=new URL("https://www.gudugudi.com/tracksensors/0351510090112551-logs/0351510090112551.json");

      Log.e("urldata", ""+url);
      Log.e("url1", ""+url1);
      HttpURLConnection httpURLConnection =(HttpURLConnection)url.openConnection();
      HttpURLConnection httpURLConnection1 =(HttpURLConnection)url1.openConnection();
      InputStream inputStream=httpURLConnection.getInputStream();
      InputStream inputStream1=httpURLConnection1.getInputStream();


      Gson gson=new Gson();
      JsonElement element = new JsonParser().parse(new InputStreamReader(inputStream));
      JsonElement element1 = new JsonParser().parse(new InputStreamReader(inputStream1));
      JSONObject jsonObject = new JSONObject(element.getAsJsonObject().toString());
      JSONObject jsonObject1 = new JSONObject(element1.getAsJsonObject().toString());

      doorStJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("SensorStat").
          getJSONArray("doorStatus");
      hrJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("SensorStat").
          getJSONArray("hour");
      minJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("SensorStat").
          getJSONArray("minute");
      voltJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("SensorStat").
          getJSONArray("voltageLevel");
      mnJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("SensorStat").
          getJSONArray("month");
      secJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("SensorStat").
          getJSONArray("seconds");
      terJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("SensorStat").
          getJSONArray("terminalStatus");
      /*voltJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("SensorStat").
          getJSONArray("voltageLevel");*/
      yrJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("SensorStat").
          getJSONArray("year");


      dateJsObjecth = jsonObject1.getJSONObject("Shoolman").getJSONObject("Histrack").
          getJSONArray("date");
      hrJsObjecth = jsonObject1.getJSONObject("Shoolman").getJSONObject("Histrack").
          getJSONArray("hour");
      latiJsObjecth = jsonObject1.getJSONObject("Shoolman").getJSONObject("Histrack").
          getJSONArray("latatitude");
      longiJsObjecth = jsonObject1.getJSONObject("Shoolman").getJSONObject("Histrack").
          getJSONArray("longitude");
      minJsObjecth = jsonObject1.getJSONObject("Shoolman").getJSONObject("Histrack").
          getJSONArray("minute");
      mnJsObjecth = jsonObject1.getJSONObject("Shoolman").getJSONObject("Histrack").
          getJSONArray("month");
      secJsObjecth = jsonObject1.getJSONObject("Shoolman").getJSONObject("Histrack").
          getJSONArray("seconds");
      spJsObjecth = jsonObject1.getJSONObject("Shoolman").getJSONObject("Histrack").
          getJSONArray("speed");
      yrJsObjecth = jsonObject1.getJSONObject("Shoolman").getJSONObject("Histrack").
          getJSONArray("year");

      int i= 0;

      while( i < this.dateJsObjecth.length() ) {
        //Log.e("doorStJsObject", "******doorStJsObject*********" + this.doorStJsObject.get(i));
        Log.e("hrJsObject", "******Hour*********" + this.hrJsObject.get(i));
        Log.e("minJsObject", "******minJsObject*********" + this.minJsObject.get(i));
        Log.e("mnJsObject", "******month*********" + this.mnJsObject.get(i));
        Log.e("secJsObject", "******seconds*********" + this.secJsObject.get(i));
        //Log.e("terJsObject", "******terminalsat*********" + this.terJsObject.get(i));
        Log.e("voltJsObject", "******voltage1*********" + this.voltJsObject.get(i));
        Log.e("year", "******year*********" + this.yrJsObject.get(i));

        i++;
      }

      int j = 0;
      while( j < this.dateJsObjecth.length() ){
        Log.e("dateJsObjecth", "******dateh1*********" + this.dateJsObjecth.get(j));
        Log.e("voltJsObject", "******voltage1*********" + this.voltJsObject.get(j));
        /*Log.e("hrJsObjecth", "******hourh*********" + this.hrJsObjecth.get(j));
        Log.e("latiJsObjecth", "******latih*********" + this.latiJsObjecth.get(j));
        Log.e("longiJsObjecth", "******longih*********" + this.longiJsObjecth.get(j));
        Log.e("minJsObjecth", "******minh*********" + this.minJsObjecth.get(j));
        */
        Log.e("mnJsObjecth", "******monh*********" + this.mnJsObjecth.get(j));
        //Log.e("secJsObjecth", "******dateh*********" + this.secJsObjecth.get(j));
        Log.e("yearh", "******yearh*********" + this.yrJsObjecth.get(j));

        j++;
      }


      allSets = 1;
    } catch (MalformedURLException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    catch (JSONException e) {
      e.printStackTrace();
    }

    return null;
  }
  @Override
  protected void onPostExecute(Void aVoid) {
    super.onPostExecute(aVoid);
    //LoginActivity.data.setText(this.dataParsed);
  }

}




