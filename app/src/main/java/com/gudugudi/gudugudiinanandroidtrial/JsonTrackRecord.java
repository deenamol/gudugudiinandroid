package com.gudugudi.gudugudiinanandroidtrial;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class JsonTrackRecord  extends AsyncTask<Void, Void, String> {
    String data="";
    int allSets = 0;
    public String jsonFileName = "";
    public JSONArray dateJsObject;
    public JSONArray mnJsObject;
    public JSONArray yrJsObject;
    public JSONArray langiJsObject;
    public JSONArray latiJsObject;
    public JSONArray spdObject;
    public JSONArray hrJsObject;
    public JSONArray minJsObject;
    public JSONArray secJsObject;
    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }
    @Override
    protected String doInBackground(Void... voids) {
        try {

            // "https://www.gudugudi.com/histrck/0351510090112551-logs/0351510090112551.json"

            Log.e("logs",jsonFileName);
            URL url=new URL(jsonFileName);
            // URL url=new URL("https://api.myjson.com/bins/y29mu");


            HttpURLConnection httpURLConnection =(HttpURLConnection)url.openConnection();
            InputStream inputStream=httpURLConnection.getInputStream();
            Gson gson=new Gson();
            // Gson gson = new GsonBuilder().setDateFormat("MMM dd, yyyy").create();
           // Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
            JsonElement element = new JsonParser().parse(new InputStreamReader(inputStream));
            JSONObject jsonObject = new JSONObject(element.getAsJsonObject().toString());
            dateJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("Histrack").
                    getJSONArray("date");

            mnJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("Histrack").
                    getJSONArray("month");
           // SimpleDateFormat myFormat = new SimpleDateFormat("MMM");
           // SimpleDateFormat myFormat = new SimpleDateFormat("MMM");

          /*  SimpleDateFormat monthFormat = new SimpleDateFormat("MMM");
            String month = monthFormat .format(jsonObject.getJSONObject("Shoolman").getJSONObject("Histrack").
                getJSONArray("month"));*/

            yrJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("Histrack").
                    getJSONArray("year");
            langiJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("Histrack").
                    getJSONArray("longitude");
            latiJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("Histrack").
                    getJSONArray("latatitude");
            spdObject = jsonObject.getJSONObject("Shoolman").getJSONObject("Histrack").
                    getJSONArray("speed");
            hrJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("Histrack").
                    getJSONArray("hour");
            minJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("Histrack").
                    getJSONArray("minute");
            secJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("Histrack").
                    getJSONArray("seconds");

            int i = 0;
//            while( i < this.dateJsObject.length() ) {
//                Log.e("logs", "******Dates*********" + this.dateJsObject.get(i));
//                Log.e("logs", "******Months*********" + this.mnJsObject.get(i));
//                Log.e("logs", "******Years*********" + this.yrJsObject.get(i));
//                Log.e("logs", "******latitude*********" + this.latiJsObject.get(i));
//                Log.e("logs", "******lonitude*********" + this.langiJsObject.get(i));
//                Log.e("logs", "******speed*********" + this.spdObject.get(i));
//                i++;
//            }
            Log.e("logs", "******AFter redainf******"+ allSets);
            allSets = 1;
            Log.e("logs", "******AFter redainf******"+ allSets);
        } catch (MalformedURLException e) {
            allSets = 1;
            e.printStackTrace();
        } catch (IOException e) {
            allSets = 1;
            e.printStackTrace();
        }
        catch (JSONException e) {
            allSets = 1;
            e.printStackTrace();
        }
        allSets = 1;
        return null;
    }
    @Override
    protected void onPostExecute(String aVoid) {
        super.onPostExecute(aVoid);
        //LoginActivity.data.setText(this.dataParsed);
    }

}