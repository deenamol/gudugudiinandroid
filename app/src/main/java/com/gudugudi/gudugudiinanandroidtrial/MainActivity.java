package com.gudugudi.gudugudiinanandroidtrial;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText pas,usr;
    private CheckBox mCheckBoxRemember;
    private SharedPreferences mPrefs;
    private  static  final  String PREFS_NAME="PrefsFile";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mPrefs = getSharedPreferences(PREFS_NAME,MODE_PRIVATE);
        usr = (EditText) findViewById( R.id.input_email);
        pas = (EditText) findViewById(R.id.input_password);

        mCheckBoxRemember =(CheckBox) findViewById(R.id.checkBoxlogin);

        getPreferencesData();

    }

    private void getPreferencesData() {

   SharedPreferences sp = getSharedPreferences(PREFS_NAME,MODE_PRIVATE);
   if(sp.contains("pref_name")){
       String u = sp.getString("pref_name", "not found.");
       usr.setText(u.toString());
   }
   if(sp.contains("pref_pass")){
       String p = sp.getString("pref_pass","not found.");
       pas.setText(p.toString());
   }
   if (sp.contains("pref_check")){
       Boolean b = sp.getBoolean("pref_check" , false);
       mCheckBoxRemember.setChecked(b);
   }
    }

    public void ShowLogin(View view) {
        String username = usr.getText().toString();
        String password = pas.getText().toString();

        DBUserAdapter bg = new DBUserAdapter(this);
        bg.execute(username,password);
        if(mCheckBoxRemember.isChecked())
        {
        Boolean boolIsChecked = mCheckBoxRemember.isChecked();
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString("pref_name",usr.getText().toString());
        editor.putString("pref_pass",pas.getText().toString());
        editor.putBoolean("pref_check",boolIsChecked);
        editor.apply();
            Toast.makeText(getApplicationContext(),"settings have been saved." , Toast.LENGTH_SHORT).show();
        }else
        {
        mPrefs.edit().clear().apply();
        }
        usr.getText().clear();
        pas.getText().clear();
    }
}
