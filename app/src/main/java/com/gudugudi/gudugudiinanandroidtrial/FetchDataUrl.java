package com.gudugudi.gudugudiinanandroidtrial;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FetchDataUrl {

    @SerializedName("Shoolman")
    @Expose
    private Shoolman shoolman;

    public Shoolman getShoolman() {
        return shoolman;
    }

    public void setShoolman(Shoolman shoolman) {
        this.shoolman = shoolman;
    }

}
