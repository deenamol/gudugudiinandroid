package com.gudugudi.gudugudiinanandroidtrial;


import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import org.w3c.dom.Element;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;


public class DBUserAdapter extends AsyncTask <String, Void,String> {

  TextView textview;
  AlertDialog dialog;
  Context context;
  public Boolean login = false;
  public DBUserAdapter(Context context)
  {
    this.context = context;
  }

  @Override
  protected void onPreExecute() {
    dialog = new AlertDialog.Builder(context).create();
    dialog.setTitle("Login Status");
  }
  @Override
  protected void onPostExecute(String s) {
   // dialog.setMessage(s);
   // dialog.show();
//    if(s.contains("login successful"))
//    {
//      Intent intent_name = new Intent();
//      intent_name.setClass(context.getApplicationContext(),SuccessActivity.class);
//      context.startActivity(intent_name);
//    }
  }
  @Override
  protected String doInBackground(String... voids) {
    String result = "";
    String user = voids[0];
    String pass = voids[1];

    //String connstr = "http://yourIPhere:8080/login.php";
    String connstr = " https://www.gudugudi.com/and_login.php";

    try {
      URL url = new URL(connstr);
      HttpURLConnection http = (HttpURLConnection) url.openConnection();
      http.setRequestMethod("POST");
      http.setDoInput(true);
      http.setDoOutput(true);

      OutputStream ops = http.getOutputStream();
      BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(ops,"UTF-8"));
      String data = URLEncoder.encode("username","UTF-8")+"="+URLEncoder.encode(user,"UTF-8")
              +"&&"+URLEncoder.encode("password","UTF-8")+"="+URLEncoder.encode(pass,"UTF-8");
      writer.write(data);
      writer.flush();
      writer.close();
      ops.close();

      InputStream ips = http.getInputStream();
      BufferedReader reader = new BufferedReader(new InputStreamReader(ips,"ISO-8859-1"));
      String line ="";
      while ((line = reader.readLine()) != null)
      {
        result += line;
      }
     Log.d("her gos",result);
      String[] strings = result.split(";");
      Log.d("custmerId",strings[0]);
      Log.d("VehicleNumbers",strings[1]);
      Log.d("IMEIS",strings[2]);
     // Element eElement = (Element) nNode;
      // Set the texts into TextViews from item nodes
      // Get the Vehicle List
     // textview.setText(textview.getText() + "VehicleList : " + getNode("vehicleList", eElement)
       //       + "\n" + "\n");

      Intent intent_name = new Intent();
      intent_name.setClass(context.getApplicationContext(),LoginActivity.class);
      intent_name.putExtra("Customer_id", strings[0]);
      intent_name.putExtra("DUINumber", strings[1]);
      intent_name.putExtra("IMEINUmbers", strings[2]);
      context.startActivity(intent_name);
      reader.close();
      ips.close();
      http.disconnect();
      return result;

    } catch (MalformedURLException e) {
      result = e.getMessage();
    } catch (IOException e) {
      result = e.getMessage();
    }


    return result;
  }
}
