package com.gudugudi.gudugudiinanandroidtrial;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonData1  extends MainActivity  {

  String data="";
  int allSets = 0;
  public JSONArray dateJsObject;
  public JSONArray mnJsObject;
  public JSONArray yrJsObject;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);


  }





  protected Void doInBackground(Void... voids) {
    try {

      java.net.URL url=new URL("https://www.gudugudi.com/histrck/0351510090112551-logs/0351510090112551.json");
      // URL url=new URL("https://api.myjson.com/bins/y29mu");


      HttpURLConnection httpURLConnection =(HttpURLConnection)url.openConnection();
      InputStream inputStream=httpURLConnection.getInputStream();

      Gson gson=new Gson();
      JsonElement element = new JsonParser().parse(new InputStreamReader(inputStream));
      JSONObject jsonObject = new JSONObject(element.getAsJsonObject().toString());
      dateJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("Histrack").getJSONArray("date");
      mnJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("Histrack").getJSONArray("month");
      yrJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("Histrack").getJSONArray("year");
      int i = 0;
      while( i < this.dateJsObject.length() ) {
        Log.e("logs", "******Dates*********" + this.dateJsObject.get(i));
        Log.e("logs", "******Months*********" + this.mnJsObject.get(i));
        Log.e("logs", "******Years*********" + this.yrJsObject.get(i));
        i++;
      }

      allSets = 1;
    } catch (MalformedURLException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    catch (JSONException e) {
      e.printStackTrace();
    }

    return null;
  }


  }


