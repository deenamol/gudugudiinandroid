package com.gudugudi.gudugudiinanandroidtrial;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Shoolman {

    @SerializedName("Histrack")
    @Expose
    private Histrack histrack;

    public Histrack getHistrack() {
        return histrack;
    }

    public void setHistrack(Histrack histrack) {
        this.histrack = histrack;
    }


    @SerializedName("IngniStat")
    @Expose
    private IngniStat ingniStat;

    public IngniStat getIngniStat() {
        return ingniStat;
    }

    public void setIngniStat(IngniStat ingniStat) {
        this.ingniStat = ingniStat;
    }

    @SerializedName("SensorStat")
    @Expose
    private SensorStat sensorStat;

    public SensorStat getSensorStat() {
        return sensorStat;
    }

    public void setSensorStat(SensorStat sensorStat) {
        this.sensorStat = sensorStat;
    }


}
