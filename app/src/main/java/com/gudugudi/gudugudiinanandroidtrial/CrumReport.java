package com.gudugudi.gudugudiinanandroidtrial;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.SphericalUtil;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Locale;

public class CrumReport extends MainActivity  implements OnMapReadyCallback, GoogleMap.OnCameraMoveStartedListener, GoogleMap.OnCameraIdleListener {
    Button click;
    public static TextView data;

    GoogleMap mMap;
    private LatLng sydney1;
    private LatLng BANGALORE;
    private int iconRadiusPixels;
    int threadReles = 0;
    private  JsonTrackRecord jsonTrackRcrd;
    int numberVchicles;
    String ImeiNumber;
    Polyline poly;
    String IMEiArray[];
    String vehicleNumber[];
    String RecDate[];
    String RecMonth[];
    String RecYear[];
    String Latitude[];
    String Lanitude[];
    String Speed[];
    String DHour[];
    String DMinute[];
    String DSecond[];
    int currentIndexSet;
    int vehicleIndexSet;
    View datGbV;
    int firstTymDateClick;
    int loadIndex = 0;
    float camerzoomLevel = 10f;
    int loadAfterVehicle = 0;
    String MonthSet[] = {"jan","Jan","Feb","Mar","Apr","May","Jun",
            "Jul","Aug","Sep","Oct","Nov","Dec"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crum);


        IMEiArray = getIntent().getStringArrayExtra("IMEINumberArray");
        vehicleNumber = getIntent().getStringArrayExtra("VehicleNumbers");
        vehicleIndexSet = 0;
        firstTymDateClick =0;
        currentIndexSet = 0;
        String URL = "hi";
        new DownloadXML1().execute(URL);

        numberLiners();

    }



    private class DownloadXML1 extends AsyncTask<String, Void, Void> {

        private Object view;

        @Override

        protected Void doInBackground(String... Url) {



            int arrayIndex = 0;

            String connstr = " https://www.gudugudi.com/and_getHisDates.php";
            String result = "";
            int i = 0;
            try {

                result = "";
                URL url = new URL(connstr);
                HttpURLConnection http = (HttpURLConnection) url.openConnection();
                http.setRequestMethod("POST");
                http.setDoInput(true);
                http.setDoOutput(true);

                OutputStream ops = http.getOutputStream();

                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(ops, "UTF-8"));
                String data = URLEncoder.encode("imei", "UTF-8") + "=" + URLEncoder.encode(IMEiArray[vehicleIndexSet], "UTF-8");
                writer.write(data);
                writer.flush();
                writer.close();
                ops.close();

                InputStream ips = http.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(ips, "ISO-8859-1"));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    result += line;
                }
                if(result.length() < 1){
                    return  null;
                }
                Log.d("her gos", result);
                String[] strings = result.split(";");
                RecYear = strings[0].split(",");
                RecMonth = strings[1].split(",");
                RecDate = strings[2].split(",");



                Log.d("her gos", RecDate[0]);




            } catch (MalformedURLException e) {
                result = e.getMessage();
            } catch (IOException e) {
                result = e.getMessage();
            }
            return null;

        }


        protected void onPostExecute(Void args) {

            Log.d("After execution", RecDate[0]);
            currentIndexSet = RecDate.length-1;
            createLeftSlideDates();

            firstTymDateClick =0;
            loadIndex = 0;
            String URL = "hi";
            new DownloadHisData().execute("0");

            //  stUpMaps();
        }
    }
    private class DownloadHisData extends AsyncTask<String, Void, Void> {

        private Object view;

        @Override

        protected Void doInBackground(String... Url) {




            int arrayIndex = 0;
            //get number feild
            // for (int i = 0; i < trucKame.getLength(); i++) {
            // for (int i = 0; i < VechicleNumbers.length; i++) {

            String connstr = " https://www.gudugudi.com/and_getTrckRecordByImei.php";
            String result = "";
            int i = 0;
            try {

                result = "";
                URL url = new URL(connstr);
                HttpURLConnection http = (HttpURLConnection) url.openConnection();
                http.setRequestMethod("POST");
                http.setDoInput(true);
                http.setDoOutput(true);

                OutputStream ops = http.getOutputStream();

                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(ops, "UTF-8"));
                String data = URLEncoder.encode("imei", "UTF-8") + "=" + URLEncoder.encode(IMEiArray[vehicleIndexSet], "UTF-8")
                        +"&&"+URLEncoder.encode("date","UTF-8")+"="+URLEncoder.encode(RecDate[currentIndexSet],"UTF-8")
                        +"&&"+URLEncoder.encode("month","UTF-8")+"="+URLEncoder.encode(RecMonth[currentIndexSet],"UTF-8")
                        +"&&"+URLEncoder.encode("year","UTF-8")+"="+URLEncoder.encode(RecYear[currentIndexSet],"UTF-8");
                writer.write(data);
                writer.flush();
                writer.close();
                ops.close();

                InputStream ips = http.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(ips, "ISO-8859-1"));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    result += line;
                }
                if(result.length() < 1){
                    return  null;
                }
                Log.d("Now Histrack data", result);
                String[] strings = result.split(";");
                Latitude = strings[0].split(",");
                Lanitude= strings[1].split(",");
                Speed = strings[2].split(",");
                DHour = strings[3].split(",");
                DMinute = strings[4].split(",");
                DSecond = strings[5].split(",");
                // Log.d("her gos", RecDate[0]);




            } catch (MalformedURLException e) {
                result = e.getMessage();
            } catch (IOException e) {
                result = e.getMessage();
            }
            return null;

        }


        protected void onPostExecute(Void args) {

          //  currentIndexSet = DHour.length-1;
            stUpMaps();
            //  stUpMaps();
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        String address;// = addresses.get(0).getAddressLine(0);
        String city; //= addresses.get(0).getAddressLine(1);
        String country; //= addresses.get(0).getAddressLine(2);

        mMap.setOnCameraMoveStartedListener(this);
        mMap.setOnCameraIdleListener(this);
        mMap.getUiSettings().setAllGesturesEnabled(false);

        mMap.getUiSettings().setZoomControlsEnabled(true);


        int inx = 0;



        // Add a marker in Sydney and move the camera
        sydney1 = new LatLng(Double.parseDouble(Latitude[0].toString()),
                Double.parseDouble(Lanitude[0].toString()));
        BANGALORE =new LatLng(Double.parseDouble(Latitude[Latitude.length-1].toString()),
                Double.parseDouble(Lanitude[Latitude.length-1].toString()));

        // BitmapDescriptor iconDescr = BitmapDescriptorFactory.fromResource(R.drawable.truck);

        int height = 55;
        int width = 35;

        BitmapDrawable bitmapdrawstart=(BitmapDrawable)getResources().getDrawable(R.drawable.start_s);
        BitmapDrawable bitmapdrawend=(BitmapDrawable)getResources().getDrawable(R.drawable.stop_s);
        Bitmap b1=bitmapdrawstart.getBitmap();
        Bitmap b2 = getBitmap(this, R.drawable.carimg);
        Bitmap smallMarkerstart = Bitmap.createScaledBitmap(b1, width, height, false);
        Bitmap smallMarkerend = Bitmap.createScaledBitmap(b2, width, height, false);

        try {
            Geocoder geoCoder = new Geocoder(getApplicationContext(),
                    Locale.getDefault());
            // while(jsonTrackRcrd.allSets == 0){
            // ;
            // Log.e("latitude&longi", "onMapReady: "+latList[j] +" ." + langList[j]);
            //}


            List<Address> addresses;


            addresses = geoCoder.getFromLocation(Double.parseDouble(Latitude[0].toString()),
                    Double.parseDouble(Lanitude[0].toString()), 1);

            address = addresses.get(0).getAddressLine(0);
            city = addresses.get(0).getAddressLine(1);
            country = addresses.get(0).getAddressLine(2);

            double angle = 130.0; // rotation angle
            double x = Math.sin(-angle * Math.PI / 180) * 0.5 + 0.5;
            double y = -(Math.cos(-angle * Math.PI / 180) * 0.5 - 0.5);


            mMap.addMarker(new MarkerOptions().position(sydney1).infoWindowAnchor((float)x, (float)y).anchor(0.5F, 0.5F)
                    .draggable(false).visible(true).title("Start").
                            snippet(String.valueOf("Address--"+address + "\n" + city + "\n" + country +"\n"
                                    +"\n Reached At-->"+DHour[0]+":"+DMinute[0]))
                    .icon(BitmapDescriptorFactory.fromBitmap(smallMarkerend))).showInfoWindow();





        } catch (IOException e) {
            e.printStackTrace();
        }




        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(BANGALORE, 10F));




    }
    private class DownloadXML12 extends AsyncTask<String, Void, Void> {

        private Object view;

        @Override

        protected Void doInBackground(String... Url) {

            int i = 0;
            i++;

           return null;
        }


        protected void onPostExecute(Void args) {



            loadIndex++;
            nextPosition();

        }
    }

    public void nextPosition() {
       // mMap.clear();
        if(Lanitude.length <= 1)
            return ;
        if(loadIndex > Lanitude.length -1 )
            return;
        mMap.clear();
        sydney1 = new LatLng(Double.parseDouble(Latitude[loadIndex].toString()),
                Double.parseDouble(Lanitude[loadIndex].toString()));

        String address;// = addresses.get(0).getAddressLine(0);
        String city; //= addresses.get(0).getAddressLine(1);
        String country; //= addresses.get(0).getAddressLine(2);
        int height = 55;
        int width = 35;

        BitmapDrawable bitmapdrawstart=(BitmapDrawable)getResources().getDrawable(R.drawable.start_s);
        BitmapDrawable bitmapdrawend=(BitmapDrawable)getResources().getDrawable(R.drawable.stop_s);
        Bitmap b1=bitmapdrawstart.getBitmap();
        Bitmap b2 = getBitmap(this, R.drawable.carimg);
        Bitmap smallMarkerstart = Bitmap.createScaledBitmap(b1, width, height, false);
        Bitmap smallMarkerend = Bitmap.createScaledBitmap(b2, width, height, false);
        try {
            Geocoder geoCoder = new Geocoder(getApplicationContext(),
                    Locale.getDefault());
        List<Address> addresses;


        addresses = geoCoder.getFromLocation(Double.parseDouble(Latitude[loadIndex].toString()),
                Double.parseDouble(Lanitude[loadIndex].toString()), 1);

        address = addresses.get(0).getAddressLine(0);
        city = addresses.get(0).getAddressLine(1);
        country = addresses.get(0).getAddressLine(2);

            float xn, yn, test = 180;
            if(GetBearing(new LatLng(Double.parseDouble(Latitude[Latitude.length - 2].toString()),
                    Double.parseDouble(Lanitude[Latitude.length - 2].toString())),new LatLng(Double.parseDouble(Latitude[Latitude.length - 1].toString()),
                    Double.parseDouble(Lanitude[Latitude.length - 1].toString()))) < 90) {
                xn = 0f;
                yn = 3f;
            }
            else if(GetBearing(new LatLng(Double.parseDouble(Latitude[Latitude.length - 2].toString()),
                    Double.parseDouble(Lanitude[Latitude.length - 2].toString())),new LatLng(Double.parseDouble(Latitude[Latitude.length - 1].toString()),
                    Double.parseDouble(Lanitude[Latitude.length - 1].toString()))) < 180) {

                ;            xn = 3f;
                yn = 0f;
            } else if(GetBearing(new LatLng(Double.parseDouble(Latitude[Latitude.length - 2].toString()),
                    Double.parseDouble(Lanitude[Latitude.length - 2].toString())),new LatLng(Double.parseDouble(Latitude[Latitude.length - 1].toString()),
                    Double.parseDouble(Lanitude[Latitude.length - 1].toString())))  < 270) {

                xn = -3f;
                yn = 0f;
            }
            else
            {
                xn = 0f;
                yn = -3f;
            }
            double angle = 130.0; // rotation angle
            double x = Math.sin(-angle * Math.PI / 180) * 0.8 + 0.8;
            double y = -(Math.cos(-angle * Math.PI / 180) * 0.2 - 0.2);


        mMap.addMarker(new MarkerOptions().position(sydney1).infoWindowAnchor((float)x, (float)y).
                anchor(0.5F, 0.5F).rotation(GetBearing(new LatLng(Double.parseDouble(Latitude[loadIndex-1].toString()),
                Double.parseDouble(Lanitude[loadIndex-1].toString())),new LatLng(Double.parseDouble(Latitude[loadIndex].toString()),
                Double.parseDouble(Lanitude[loadIndex].toString()))))
                .draggable(false).visible(true).title("Speed: "+Speed[loadIndex]+ "KMPH"
                +"   TIME: "+DHour[loadIndex] + "H : " + DMinute [loadIndex] + " M").
                        snippet(String.valueOf("Address--"+address + "\n" + city + "\n" + country +"\n"
                                +"\n Reached At-->"+DHour[loadIndex]+":"+DMinute[loadIndex]))
                .icon(BitmapDescriptorFactory.fromBitmap(smallMarkerend))).showInfoWindow();

        } catch (IOException e) {
            e.printStackTrace();
        }
       // mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney1, 10F));
//        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
//                sydney1, 10F);
        CameraPosition position = CameraPosition.builder()
                .target(sydney1)
                .zoom(15F)
                .tilt(30)
                .build();


        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position) ,2500, new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
                //Here you can take the snapshot or whatever you want
                if(loadIndex < Latitude.length-2)
                new DownloadXML12().execute("0");

            }


            @Override
            public void onCancel() {

            }
        });


       // mMap.set
        mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                CameraPosition cameraPosition = mMap.getCameraPosition();



                    camerzoomLevel = cameraPosition.zoom;


            }
        });




    }
    @Override
    public void onCameraMoveStarted(int reason) {

    }

    @Override
    public void onCameraIdle() {
//        if (this.poly != null) {
//            this.poly.remove();
//            this.poly = null;
//        }
       // Toast.makeText(this, "Camera Idel", Toast.LENGTH_SHORT).show();
    }

    public double totalDistanceTravalled() {

        int i = 0;
        double distance = 0 ;
        while( i < Latitude .length -2) {

            distance =  distance + setDistance(i);
            i++;
        }

        return distance;

    }
    public double setDistance(int i) {

        return  lngDistance(Double.parseDouble(Latitude [i].toString()),Double.parseDouble(Lanitude [i].toString())
                , Double.parseDouble(Latitude [i + 1].toString()),
                Double.parseDouble(Lanitude [i + 1].toString()) );
    }
    public double lngDistance(double lat1,double lon1,double lat2,double lon2) {
        int R = 6371; // km (change this constant to get miles)
        double dLat = (lat2-lat1) * Math.PI / 180;
        double dLon = (lon2-lon1) * Math.PI / 180;
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(lat1 * Math.PI / 180 ) * Math.cos(lat2 * Math.PI / 180 ) *
                        Math.sin(dLon/2) * Math.sin(dLon/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double d = R * c;
    /*	if (d>1) {
               return d; //+"km";
         }
    	else if (d<=1) {
           return d;/// +"m";
       } */
        return d;
    }



    public void showCustomPolyline() {


        sydney1 = new LatLng(Double.parseDouble(Latitude[0].toString()),
                Double.parseDouble(Lanitude[0].toString()));
        BANGALORE =new LatLng(Double.parseDouble(Latitude[Latitude.length-1].toString()),
                Double.parseDouble(Lanitude[Latitude.length-1].toString()));


        //Get projection
        Projection proj = mMap.getProjection();
        //Get a point on the screen that corresponds to first marker
        Point p = proj.toScreenLocation(BANGALORE);

        //Lets create another point that is shifted on number of pixels iqual to icon radius
        //This point is located on circle border
        Point b = new Point(p.x + iconRadiusPixels, p.y);
        //Get the LatLng for a point on the circle border
        LatLng l = proj.fromScreenLocation(b);
        //Calculate the radius of the icon (distance between center and point on the circle border)
        double r = SphericalUtil.computeDistanceBetween(sydney1, l);
        //Calculate heading from point 1 to point 2 and from point 2 to point 1
        double heading1 = SphericalUtil.computeHeading(sydney1, BANGALORE);
        double heading2 = SphericalUtil.computeHeading(BANGALORE, sydney1);


        //Create polyline
        PolylineOptions options = new PolylineOptions();
        //Calculate real position where the polyline starts and ends taking into account radius and heading
        //  LatLng pos1 = SphericalUtil.computeOffset(sydney1, r, heading1);
        //  LatLng pos2 = SphericalUtil.computeOffset(BANGALORE, r, heading2);

        int setId = 0;
        for(int i =0;  i < loadIndex ; i++ ) {

            LatLng pos1 =  new LatLng(Double.parseDouble(Latitude[i].toString()),
                    Double.parseDouble(Lanitude[i].toString()));
            if(i < Latitude.length-1 ) {
                LatLng pos2 = new LatLng(Double.parseDouble(Latitude[i + 1].toString()),
                        Double.parseDouble(Lanitude[i + 1].toString()));

                int tsetTym = Integer.parseInt(DHour[setId])*60
                        + Integer.parseInt(DMinute[setId]) + 10;
                int mTym = Integer.parseInt(DHour[i])*60
                        + Integer.parseInt(DMinute[i]);
                if(mTym > tsetTym) {
                    setId = i;
                    DrawArrowHead(mMap, pos1, pos2);
                }

            }

            options.add(pos1);
        }


        poly = mMap.addPolyline(options.width(8).color(Color.RED).geodesic(false));



    }


    private final double degreesPerRadian = 180.0 / Math.PI;

    private void DrawArrowHead(GoogleMap mMap, LatLng from, LatLng to){
        // obtain the bearing between the last two points
        double bearing = GetBearing(from, to);

        // round it to a multiple of 3 and cast out 120s
        double adjBearing = Math.round(bearing / 3) * 3;
        while (adjBearing >= 120) {
            adjBearing -= 120;
        }

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // Get the corresponding triangle marker from Google
        URL url;
        Bitmap image = null;

        try {
            url = new URL("http://www.google.com/intl/en_ALL/mapfiles/dir_" + String.valueOf((int)adjBearing) + ".png");
            try {
                image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (image != null){

            // Anchor is ratio in range [0..1] so value of 0.5 on x and y will center the marker image on the lat/long
            float anchorX = 0.5f;
            float anchorY = 0.5f;

            int offsetX = 0;
            int offsetY = 0;

            // images are 24px x 24px
            // so transformed image will be 48px x 48px

            //315 range -- 22.5 either side of 315
            if (bearing >= 292.5 && bearing < 335.5){
                offsetX = 24;
                offsetY = 24;
            }
            //270 range
            else if (bearing >= 247.5 && bearing < 292.5){
                offsetX = 24;
                offsetY = 12;
            }
            //225 range
            else if (bearing >= 202.5 && bearing < 247.5){
                offsetX = 24;
                offsetY = 0;
            }
            //180 range
            else if (bearing >= 157.5 && bearing < 202.5){
                offsetX = 12;
                offsetY = 0;
            }
            //135 range
            else if (bearing >= 112.5 && bearing < 157.5){
                offsetX = 0;
                offsetY = 0;
            }
            //90 range
            else if (bearing >= 67.5 && bearing < 112.5){
                offsetX = 0;
                offsetY = 12;
            }
            //45 range
            else if (bearing >= 22.5 && bearing < 67.5){
                offsetX = 0;
                offsetY = 24;
            }
            //0 range - 335.5 - 22.5
            else {
                offsetX = 12;
                offsetY = 24;
            }

            Bitmap wideBmp;
            Canvas wideBmpCanvas;
            Rect src, dest;

            // Create larger bitmap 4 times the size of arrow head image
            wideBmp = Bitmap.createBitmap(image.getWidth() * 2, image.getHeight() * 2, image.getConfig());

            wideBmpCanvas = new Canvas(wideBmp);

            src = new Rect(0, 0, image.getWidth(), image.getHeight());
            dest = new Rect(src);
            dest.offset(offsetX, offsetY);

            wideBmpCanvas.drawBitmap(image, src, dest, null);

            mMap.addMarker(new MarkerOptions()
                    .position(to)
                    .icon(BitmapDescriptorFactory.fromBitmap(wideBmp))
                    .anchor(anchorX, anchorY));
        }
    }

    private float GetBearing(LatLng from, LatLng to){
        double lat1 = from.latitude * Math.PI / 180.0;
        double lon1 = from.longitude * Math.PI / 180.0;
        double lat2 = to.latitude * Math.PI / 180.0;
        double lon2 = to.longitude * Math.PI / 180.0;

        // Compute the angle.
        double angle = - Math.atan2( Math.sin( lon1 - lon2 ) * Math.cos( lat2 ), Math.cos( lat1 ) * Math.sin( lat2 ) - Math.sin( lat1 ) * Math.cos( lat2 ) * Math.cos( lon1 - lon2 ) );

        if (angle < 0.0)
            angle += Math.PI * 2.0;

        // And convert result to degrees.
        angle = angle * degreesPerRadian;

        return (float) angle;
    }

    protected  void numberLiners() {



        final LinearLayout lm = (LinearLayout) findViewById(R.id.numliners);


//        try {

        // params.addRule(RelativeLayout.BELOW);

        //ll.setOrientation(RelativeLayout.HORIZONTAL);

        // ll.setLayoutParams(params);




        for ( int i = 0 ; i < vehicleNumber.length ; i++) {


            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);


            //      params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

            // create the layout params that will be used to define how your
            // button will be displayed

            LinearLayout lnrLay = new LinearLayout(this);
            //   Create LinearLayout

            RelativeLayout ll = new RelativeLayout(this);

            //  ll.setOrientation(RelativeLayout.HORIZONTAL);
            // Create TextView
            Button btn = new Button(this);
            // Give button an ID
            btn.setId(i);
            btn.setText(vehicleNumber[i]);
            //   btn.setWidth((lm.getWidth() * 35* 4) / 100);
            // set the layoutParams on the button
            btn.setLayoutParams(params);
            if(i != 0)
                btn.setBackgroundResource(R.drawable.roundedbutton1);
            else
                btn.setBackgroundResource(R.drawable.roundbuttongh);
            ((btn)).setTextColor(Color.parseColor("#ffffff"));
            // btn.setShadowLayer(3, 0, 0, Color.WHITE);
            //btn.setAlpha(0.3f);
            final int index = i;





            //Add button to LinearLayout defined in XML
            lm.addView(btn);


            btn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {



                    int preInd = vehicleIndexSet;

                    vehicleIndexSet = v.getId();
                    currentIndexSet = 0;
                    Button  button = (Button) findViewById(preInd);// datGbV = v;
                    button.setBackgroundResource(R.drawable.roundedbutton1);
                    v.setBackgroundResource(R.drawable.roundbuttongh);


                    final LinearLayout lm = (LinearLayout) findViewById(R.id.liners);
                    lm.removeAllViews();

                        mMap.clear();


                      //      poly.remove();
                        String URL = "hi";
                       new DownloadXML1().execute(URL);



                }
            });

        }
    }


    protected void createLeftSlideDates() {

        final LinearLayout lm = (LinearLayout) findViewById(R.id.liners);
        //lm.setAlpha(0.5f);
        lm.getBackground().setAlpha(50);






        for ( int i = RecDate.length-1 ; i >= 0 ; i--) {


            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);




            LinearLayout lnrLay = new LinearLayout(this);
            //   Create LinearLayout

            RelativeLayout ll = new RelativeLayout(this);

            // ll.setOrientation(RelativeLayout.HORIZONTAL);
            // Create TextView
            Button btn = new Button(this);
            // Give button an ID
            btn.setId(i);

            String dt = RecDate[i] + "-" + MonthSet[Integer.parseInt(RecMonth[i].toString())] + "-" +RecYear[i];//(RecDate[i] + "-" +RecMonth[i] + "-" +RecYear[i]);

            // set the layoutParams on the button
            btn.setText(dt);
            //  if(currentIndexSet == i)

            btn.setLayoutParams(params);
            if(RecDate.length-1 != i)
                btn.setBackgroundResource(R.drawable.roundedbutton1);
            else
                btn.setBackgroundResource(R.drawable.roundbuttongh);

            ((btn)).setTextColor(Color.parseColor("#ffffff"));
            // btn.setShadowLayer(5, 0, 0, Color.WHITE);
            final int index = i;


            lm.addView(btn);

            btn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    mMap.clear();
                    int preInd = currentIndexSet;




                    currentIndexSet = v.getId();



                    // datGbV.setBackgroundResource(R.drawable.roundedbutton1);
                    Button  button = (Button) findViewById(preInd);// datGbV = v;
                    button.setBackgroundResource(R.drawable.roundedbutton1);

                    v.setBackgroundResource(R.drawable.roundbuttongh);

                    String URL = "hi";
                    new DownloadHisData().execute("0");

//
//                    TranslateAnimation upmotionleft = new TranslateAnimation(0, 0, 0, 600);
//                    upmotionleft.setDuration(2000);
//                    upmotionleft.setFillEnabled(true);
//                    upmotionleft.setAnimationListener(new Animation.AnimationListener()
//                    {
//                        @Override
//                        public void onAnimationStart(Animation animation)
//                        {}
//                        @Override
//                        public void onAnimationEnd(Animation animation)
//                        {
//                            //sets to wherever I want the final object to be
//                            View object =null;
//                            ViewGroup.MarginLayoutParams marginParams = new ViewGroup.MarginLayoutParams(object.getLayoutParams());
//                            //marginParams.setMargins(left, top-hol2, left, 0);
//                            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(marginParams);
//                            object.setLayoutParams(layoutParams);
//
//                            //starts next animation
//                            Animation nextAnimation = null;
//                            object.startAnimation(nextAnimation);
//                        }
//
//                        @Override
//                        public void onAnimationRepeat(Animation animation)
//                        {}
//                    });
//
//                    View object = null;
//                    object.startAnimation(upmotionleft);


                }

            });

        }


    }

    protected  void stUpMaps() {
        //      //   Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.main_activity_map);


        mapFragment.getView().setClickable(false);
        //Let's calculate what is a radius in screen pixels for our icon
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        Drawable m_icon = ResourcesCompat.getDrawableForDensity(getResources(), R.drawable.truck, dm.densityDpi, null);
        int height = ((BitmapDrawable) m_icon).getBitmap().getHeight();
        int width = ((BitmapDrawable) m_icon).getBitmap().getWidth();

        iconRadiusPixels = width / 2;

        mapFragment.getMapAsync(this);
        new DownloadXML12().execute("0");
    }
    public Bitmap getBitmap(VectorDrawable vectorDrawable) {
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        vectorDrawable.draw(canvas);
        return bitmap;
    }

    public Bitmap getBitmap(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (drawable instanceof BitmapDrawable) {
            return BitmapFactory.decodeResource(context.getResources(), drawableId);
        } else if (drawable instanceof VectorDrawable) {
            return getBitmap((VectorDrawable) drawable);
        } else {
            throw new IllegalArgumentException("unsupported drawable type");
        }
    }




}
