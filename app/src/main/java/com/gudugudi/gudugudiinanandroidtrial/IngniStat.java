
package com.gudugudi.gudugudiinanandroidtrial;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IngniStat {

    @SerializedName("hour")
    @Expose
    private List<Integer> hour = null;
    @SerializedName("ingnition")
    @Expose
    private List<String> ingnition = null;
    @SerializedName("minute")
    @Expose
    private List<Integer> minute = null;
    @SerializedName("month")
    @Expose
    private List<Integer> month = null;
    @SerializedName("seconds")
    @Expose
    private List<Integer> seconds = null;
    @SerializedName("year")
    @Expose
    private List<Integer> year = null;

    public List<Integer> getHour() {
        return hour;
    }

    public void setHour(List<Integer> hour) {
        this.hour = hour;
    }

    public List<String> getIngnition() {
        return ingnition;
    }

    public void setIngnition(List<String> ingnition) {
        this.ingnition = ingnition;
    }

    public List<Integer> getMinute() {
        return minute;
    }

    public void setMinute(List<Integer> minute) {
        this.minute = minute;
    }

    public List<Integer> getMonth() {
        return month;
    }

    public void setMonth(List<Integer> month) {
        this.month = month;
    }

    public List<Integer> getSeconds() {
        return seconds;
    }

    public void setSeconds(List<Integer> seconds) {
        this.seconds = seconds;
    }

    public List<Integer> getYear() {
        return year;
    }

    public void setYear(List<Integer> year) {
        this.year = year;
    }

}
