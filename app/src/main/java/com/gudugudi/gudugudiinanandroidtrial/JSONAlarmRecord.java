package com.gudugudi.gudugudiinanandroidtrial;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class JSONAlarmRecord  extends AsyncTask<Void,Void,Void>  {
    String data="";
    String dataParsed="";
    String singleParsed="";
    int allSets = 0;
    public String jsonAlaramFileName = "";
    public JSONArray alarmTypeJsObject;
    public JSONArray dateJsObject;
    public JSONArray mnJsObject;
    public JSONArray yrJsObject;
    public JSONArray longiJsObject;
    public JSONArray latiJsObject;
    public JSONArray spdObject;
    public JSONArray hrJsObject;
    public JSONArray minJsObject;
    public JSONArray secJsObject;
    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }
    @Override
    protected Void doInBackground(Void... voids) {
        try {

            // "https://www.gudugudi.com/histrck/0351510090112551-logs/0351510090112551.json"

            Log.e("logs",jsonAlaramFileName);
            URL url=new URL(jsonAlaramFileName);
            // URL url=new URL("https://api.myjson.com/bins/y29mu");


            HttpURLConnection httpURLConnection =(HttpURLConnection)url.openConnection();
            InputStream inputStream=httpURLConnection.getInputStream();

            Gson gson=new Gson();
            JsonElement element = new JsonParser().parse(new InputStreamReader(inputStream));
            JSONObject jsonObject = new JSONObject(element.getAsJsonObject().toString());
            alarmTypeJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("Histrack").
                getJSONArray("alarmType");

            dateJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("Histrack").
                getJSONArray("date");
            mnJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("Histrack").
                getJSONArray("month");
            hrJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("Histrack").
                getJSONArray("hour");
            latiJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("Histrack").
                getJSONArray("latatitude");
            longiJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("Histrack").
                getJSONArray("longitude");
            minJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("Histrack").
                getJSONArray("minute");
            secJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("Histrack").
                getJSONArray("seconds");
            spdObject = jsonObject.getJSONObject("Shoolman").getJSONObject("Histrack").
                getJSONArray("speed");
            yrJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("Histrack").
                getJSONArray("year");


            int i = 0;
//            while( i < this.dateJsObject.length() ) {
//                Log.e("logsalaram", "******alaram*********" + this.alarmTypeJsObject.get(i));
//                Log.e("logsalaram", "******Dates*********" + this.dateJsObject.get(i));
//                Log.e("logsalaram", "******Months*********" + this.mnJsObject.get(i));
//                Log.e("logsalaram", "******Years*********" + this.yrJsObject.get(i));
//                Log.e("logsalaram", "******latitude*********" + this.latiJsObject.get(i));
//                Log.e("logsalaram", "******lonitude*********" + this.longiJsObject.get(i));
//                Log.e("logsalaram", "******speed*********" + this.spdObject.get(i));
//                i++;
//            }

            Log.e("logs", "******AFter redainf******"+ allSets);
            allSets = 1;
            Log.e("logs", "******AFter redainf******"+ allSets);
        } catch (MalformedURLException e) {
            allSets = 1;
            e.printStackTrace();
        } catch (IOException e) {
            allSets = 1;
            e.printStackTrace();
        }
        catch (JSONException e) {
            allSets = 1;
            e.printStackTrace();
        }

        allSets = 1;
        return null;
    }
    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        //LoginActivity.data.setText(this.dataParsed);
    }

}