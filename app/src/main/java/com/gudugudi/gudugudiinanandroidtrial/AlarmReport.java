package com.gudugudi.gudugudiinanandroidtrial;
import android.content.Context;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.Display;
import android.view.View;

import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.util.Log;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.List;
import java.util.Locale;
import org.json.JSONException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AlarmReport extends MainActivity  {

  private JSONAlarmRecord jsonTrackRcrd;
  int numberVchicles;
  String ImeiNumber;

  String IMEiArray[];
  String vehicleNumber[];
  String MonthSet[] = {"jan","Jan","Feb","Mar","Apr","May","Jun",
          "Jul","Aug","Sep","Oct","Nov","Dec"};
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.alaram_list);

    IMEiArray = getIntent().getStringArrayExtra("IMEINumberArray");
    vehicleNumber = getIntent().getStringArrayExtra("VehicleNumbers");

    ImeiNumber = IMEiArray [0];

    String datTimeFileNam = "https://www.gudugudi.com/trackalarms/"+ ImeiNumber + "-logs/" + ImeiNumber + ".json";
    JsonAlaramData jsnExtarct = new JsonAlaramData();
    jsnExtarct.jsonFileName = datTimeFileNam;
    jsnExtarct.execute();


    //   jsnExtarct.getJsonData();
  //  try {

      while (jsnExtarct.allSets == 0) {
        Log.e("logs", "******In loop******"+ jsnExtarct.allSets);
      }



      numberLiners();

      if(jsnExtarct.dateJsObject == null){
          return ;
      }
    if(jsnExtarct.yrJsObject.length() > 0)
      createLeftSlideDates(jsnExtarct);
//      alarm(jsnExtarct);
//      JSONAlarmRecord jsnTrckRecord = new JSONAlarmRecord();
//      String trckFileName = "https://www.gudugudi.com/trackalarms/"+ ImeiNumber + "-logs/" + ImeiNumber;
//
//      trckFileName = trckFileName + "-" + jsnExtarct.dateJsObject.get(jsnExtarct.dateJsObject.length()-1).toString()
//              + "-" + jsnExtarct.mnJsObject.get(jsnExtarct.dateJsObject.length()-1).toString()
//              + "-" + jsnExtarct.yrJsObject.get(jsnExtarct.dateJsObject.length()-1).toString()+ ".json";
//
//      jsnTrckRecord.jsonAlaramFileName = trckFileName;
//
//      jsnTrckRecord.execute();
//      //   jsnExtarct.getJsonData();
//
//      while (jsnTrckRecord.allSets == 0) {
//        Log.e("logs", "******In loop******"+ jsnTrckRecord.allSets);
//      }
//
//      jsonTrackRcrd = jsnTrckRecord;

//    } catch (JSONException e) {
//
//    }
  }

/*

  protected void alarm(final JsonAlaramData jsnExtarct) {

    try {

      ListView listView = (ListView) findViewById(R.id.zone_list);
      List<String> items = new ArrayList<>();
      for ( int i = jsnExtarct.dateJsObject.length() -1 ; i >= 0 ; i--)
      {
        //JSONObject object= array.getJSONObject(i);
        items.add(jsnExtarct.dateJsObject.get(i).toString());

        Log.d("TAG","flag value "+items.add(jsnExtarct.dateJsObject.get(i).toString()));
      }

      ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
          android.R.layout.simple_list_item_1, items);

      if (listView != null) {
        listView.setAdapter(adapter);
      }



    } catch (JSONException e) {
      e.printStackTrace();
    }


  }
*/

  protected  void numberLiners() {



    final LinearLayout lm = (LinearLayout) findViewById(R.id.numliners);
    //lm.setAlpha(0.6f);

//        try {

    // params.addRule(RelativeLayout.BELOW);

    //ll.setOrientation(RelativeLayout.HORIZONTAL);

    // ll.setLayoutParams(params);




    for ( int i = 0 ; i < vehicleNumber.length ; i++) {


      LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
              LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);


      //      params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

      // create the layout params that will be used to define how your
      // button will be displayed

      LinearLayout lnrLay = new LinearLayout(this);
      //   Create LinearLayout

      RelativeLayout ll = new RelativeLayout(this);

      //  ll.setOrientation(RelativeLayout.HORIZONTAL);
      // Create TextView
      Button btn = new Button(this);
      // Give button an ID
      btn.setId(i);
      btn.setText(vehicleNumber[i]);
      //   btn.setWidth((lm.getWidth() * 35* 4) / 100);
      // set the layoutParams on the button
      btn.setLayoutParams(params);
      btn.setBackgroundResource(R.drawable.roundedbutton1);
      ((btn)).setTextColor(Color.parseColor("#ffffff"));
      final int index = i;




//                Toast.makeText(getApplicationContext(),
//                        "Index:"+ i ,
//                        Toast.LENGTH_LONG).show();
      //Add button to LinearLayout

      //       lnrLay .addView(btn);
//                lnrLay.addView(ll);
      //Add button to LinearLayout defined in XML
      lm.addView(btn);


      btn.setOnClickListener(new View.OnClickListener() {
        public void onClick(View v) {

          final LinearLayout lm = (LinearLayout) findViewById(R.id.liners);
          lm.removeAllViews();
          Log.e("vehicleList", "Cliecked on Vehicle Button ");
          int inx = index;
          ImeiNumber = IMEiArray [inx];



          String datTimeFileNam = "https://www.gudugudi.com/trackalarms/"+ ImeiNumber + "-logs/" + ImeiNumber + ".json";
          JsonAlaramData jsnExtarct = new JsonAlaramData();
          jsnExtarct.allSets = 0;
          jsnExtarct.jsonFileName = datTimeFileNam;
          jsnExtarct.execute();
          while(jsnExtarct.allSets == 0){

            Log.e("logs", "******In loop Extract******"+ jsnExtarct.allSets);

          }

          if(jsnExtarct.dateJsObject == null){
            return ;
          }

          try {

            JSONAlarmRecord jsnTrckRecord = new JSONAlarmRecord();
//        String filName = "https://www.gudugudi.com/histrck/0351510090112551-logs/0351510090112551";
//        filName = filName + "-" + jsnExtarct.dateJsObject.get(jsnExtarct.dateJsObject.length()-1).toString()
//                 + "-" + jsnExtarct.mnJsObject.get(jsnExtarct.dateJsObject.length()-1).toString()
//                + "-" + jsnExtarct.yrJsObject.get(jsnExtarct.dateJsObject.length()-1).toString()+ ".json";

            String trckFileName = "https://www.gudugudi.com/trackalarms/" + ImeiNumber + "-logs/" + ImeiNumber;

            trckFileName = trckFileName + "-" + jsnExtarct.dateJsObject.get(jsnExtarct.dateJsObject.length() - 1).toString()
                    + "-" + jsnExtarct.mnJsObject.get(jsnExtarct.dateJsObject.length() - 1).toString()
                    + "-" + jsnExtarct.yrJsObject.get(jsnExtarct.dateJsObject.length() - 1).toString() + ".json";
            jsnTrckRecord.jsonAlaramFileName = trckFileName;

            jsnTrckRecord.execute();
            //   jsnExtarct.getJsonData();


            while (jsnTrckRecord.allSets == 0) {

              Log.e("logs", "******In loop Record******"+ jsnExtarct.allSets);

            }

            if(jsnTrckRecord.alarmTypeJsObject == null){
              return ;
            }


            createLeftSlideDates(jsnExtarct);
            alarm(jsnTrckRecord);



          }catch (JSONException e) {

          }
        }
      });

    }
  }
  protected void alarm(final JSONAlarmRecord jsnExtarct) {

      final LinearLayout lm = (LinearLayout) findViewById(R.id.zone_list);

    lm.removeAllViews();
   // lm.getBackground().setAlpha(50);
    try {



      LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
              LinearLayout.LayoutParams.MATCH_PARENT,
              LinearLayout.LayoutParams.MATCH_PARENT);
     // params.setMargins(15, 0, 0, 0);

      LinearLayout lnrLay = new LinearLayout(this);
      //   Create LinearLayout

      RelativeLayout ll = new RelativeLayout(this);



      // ll.setOrientation(RelativeLayout.HORIZONTAL);
      // Create TextView
      Button btn = new Button(this);

     // Give button an ID
      btn.setId(0);

      Button btns = (Button) findViewById(R.id.head);
      btns.setText("Time" +"       |" + "Speed" + "     | " + "Alarm Type"  + "     |  " + " Location" );
      // btn.setWidth((lm.getWidth() * 140* 4) / 100);
      // set the layoutParams on the button
      Display display = ((WindowManager)getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
      int width = display.getWidth();
      int height = display.getHeight();
      btn.setLayoutParams(new LinearLayout.LayoutParams((int) (width/(1.35)), height/10));
      Log.e("(width/(1.5))", "Alaram: "+(int)(width/(1.35)) );
      btn.setLayoutParams(params);
      btn.setBackgroundResource(R.drawable.roundedbutton1);
      ((btn)).setTextColor(Color.parseColor("#ffffff"));
    //  btn.setLayoutParams(new LinearLayout.LayoutParams(350, 100));

      lm.addView(btn);


      for ( int i = jsnExtarct.alarmTypeJsObject.length() -1 ; i >= 0 ; i--) {


        LinearLayout.LayoutParams paramsa = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);



        Button btna = new Button(this);
        // Give button an ID
        btna.setId(i);

        double latitude = Double.parseDouble(jsnExtarct.latiJsObject.get(i).toString());
        double longitude = Double.parseDouble(jsnExtarct.longiJsObject.get(i).toString());
        try {
          Geocoder geo = new Geocoder(AlarmReport.this.getApplicationContext(), Locale.getDefault());
          List<Address> addresses = geo.getFromLocation(latitude, longitude, 15);

          if (addresses.size() > 0) {
            String locadd = addresses.get(0).getFeatureName() + ", " + addresses.get(0).getLocality() + ", " + addresses.get(0).getAdminArea() + ", " + addresses.get(0).getCountryName();
            //Toast.makeText(getApplicationContext(), "Address:- " + addresses.get(0).getFeatureName() + addresses.get(0).getAdminArea() + addresses.get(0).getLocality(), Toast.LENGTH_LONG).show();

        btna.setText(jsnExtarct.hrJsObject.get(i).toString() + ":" +
                        jsnExtarct.minJsObject.get(i).toString() +":" +
                        jsnExtarct.secJsObject.get(i).toString() + " | " +
                        jsnExtarct.spdObject.get(i).toString()  + "Kmh"  + " | " +
                        jsnExtarct.alarmTypeJsObject.get(i).toString() + "|" + locadd);




        btna.setBackgroundResource(R.drawable.roundedbutton1);
        ((btna)).setTextColor(Color.parseColor("#ffffff"));
       // paramsa.setMargins(0, 0, 0, 0);
        btna.setLayoutParams(paramsa);

        lm.addView(btna);


      }

    }
    catch (JSONException e) {

    }
      }



    }
    catch (Exception e) {
      e.printStackTrace(); // getFromLocation() may sometimes fail
    }
  }



  protected void createLeftSlideDates(final JsonAlaramData jsnExtarct) {


    final LinearLayout lm = findViewById(R.id.liners);
    //lm.setAlpha(0.5f);
    lm.getBackground().setAlpha(50);
    try {

      // params.addRule(RelativeLayout.BELOW);

      //ll.setOrientation(RelativeLayout.HORIZONTAL);

      // ll.setLayoutParams(params);




      for ( int i = jsnExtarct.dateJsObject.length() -1 ; i >= 0 ; i--) {


        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);


        //      params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

        // create the layout params that will be used to define how your
        // button will be displayed

        LinearLayout lnrLay = new LinearLayout(this);
        //   Create LinearLayout

        RelativeLayout ll = new RelativeLayout(this);

        // ll.setOrientation(RelativeLayout.HORIZONTAL);
        // Create TextView
        Button btn = new Button(this);
        // Give button an ID
        btn.setId(i);

       /* btn.setText(jsnExtarct.dateJsObject.get(i).toString() + "   " +
            jsnExtarct.mnJsObject.get(i).toString() + "   " + jsnExtarct.yrJsObject.get(i)
            .toString());
*/
        String dt = (jsnExtarct.dateJsObject.get(i).toString() + "-" +jsnExtarct. mnJsObject.get(i).toString()+ "-" +jsnExtarct.yrJsObject.get(i).toString());
        Log.e("dt", "doInBackground: "+dt );
// old SimpleDateFormat
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yy", Locale.ENGLISH);
// new SimpleDateFormat
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd MMM yy", Locale.ENGLISH);
        Date date = null;
        try {
          date = sdf1.parse(dt);
          String newDate = sdf2.format(date);
          System.out.println(newDate);
          Log.e("Date", newDate);
          btn.setText(newDate);
        } catch (ParseException e) {
          e.printStackTrace();
        }


     /*
      btn.setText(jsnExtarct.dateJsObject.get(i).toString() + "   " +
            jsnExtarct.mnJsObject.get(i).toString() + "   " + jsnExtarct.yrJsObject.get(i)
            .toString());*/
      //  btn.setWidth((lm.getWidth() * 35* 4) / 100);
        // set the layoutParams on the button
        btn.setLayoutParams(params);
        btn.setBackgroundResource(R.drawable.roundedbutton1);
        ((btn)).setTextColor(Color.parseColor("#ffffff"));
        final int index = i;

       lm.addView(btn);

       if(i == jsnExtarct.dateJsObject.length() -1) {
         int inx = i;
         JSONAlarmRecord jsnTrckRecord = new JSONAlarmRecord();
         String filName = "https://www.gudugudi.com/trackalarms/" + ImeiNumber + "-logs/" +
                 ImeiNumber;

         jsnTrckRecord.allSets = 0;
         filName = filName + "-" + jsnExtarct.dateJsObject.get(inx).toString() + "-"
                 + jsnExtarct.mnJsObject.get(inx).toString() + "-" +
                 jsnExtarct.yrJsObject.get(inx).toString() + ".json";
         jsnTrckRecord.jsonAlaramFileName = filName;

         jsnTrckRecord.execute();
         //   jsnExtarct.getJsonData();


         while (jsnTrckRecord.allSets == 0) {
           Log.e("logs", "******In loop******" + jsnTrckRecord.allSets);

         }


         //  jsonTrackRcrd = jsnTrckRecord;
//              if(jsnTrckRecord.alarmTypeJsObject == null){
//                                              Toast.makeText(getApplicationContext(),
//                       "Zero Value File" ,
//                        Toast.LENGTH_LONG).show();
//                return ;
//              }

         if(jsnTrckRecord.alarmTypeJsObject != null)
           alarm(jsnTrckRecord);
       }

        btn.setOnClickListener(new View.OnClickListener() {
          public void onClick(View v) {

            try {
              int inx = index;
              JSONAlarmRecord jsnTrckRecord = new JSONAlarmRecord();
              String filName = "https://www.gudugudi.com/trackalarms/"+ImeiNumber+"-logs/"+
                      ImeiNumber;

              jsnTrckRecord.allSets = 0;
              filName = filName + "-" + jsnExtarct.dateJsObject.get(inx).toString() + "-"
                  + jsnExtarct.mnJsObject.get(inx).toString()+ "-" +
                  jsnExtarct.yrJsObject.get(inx).toString()+ ".json";
              jsnTrckRecord.jsonAlaramFileName = filName;

              jsnTrckRecord.execute();
              //   jsnExtarct.getJsonData();


              while(jsnTrckRecord.allSets == 0){
                Log.e("logs", "******In loop******"+ jsnTrckRecord.allSets);

              }

            //  jsonTrackRcrd = jsnTrckRecord;
//              if(jsnTrckRecord.alarmTypeJsObject == null){
//                                              Toast.makeText(getApplicationContext(),
//                       "Zero Value File" ,
//                        Toast.LENGTH_LONG).show();
//                return ;
//              }

              if(jsnTrckRecord.alarmTypeJsObject != null)
              alarm(jsnTrckRecord);

//              Log.i("TAG", "Date :" + jsonTrackRcrd.dateJsObject.get(inx).toString()+
//                  "Month :" + jsonTrackRcrd.dateJsObject.get(inx).toString()+
//                  "Year :" + jsonTrackRcrd.dateJsObject.get(inx).toString());

//              Toast.makeText(getApplicationContext(),
//                  "Date :" + jsonTrackRcrd.dateJsObject.get(inx).toString()+
//                      "Month :" + jsonTrackRcrd.mnJsObject.get(inx).toString()+
//                      "Year :" + jsonTrackRcrd.yrJsObject.get(inx).toString(),
//                  Toast.LENGTH_LONG).show();

            } catch (JSONException e) {
            }
          }

        });


//                Toast.makeText(getApplicationContext(),
//                        "Index:"+ i ,
//                        Toast.LENGTH_LONG).show();
        //Add button to LinearLayout

        //       lnrLay .addView(btn);
//                lnrLay.addView(ll);
        //Add button to LinearLayout defined in XML
      //  lm.addView(btn);

      }

    }
    catch (JSONException e) {

    }
  }



}
