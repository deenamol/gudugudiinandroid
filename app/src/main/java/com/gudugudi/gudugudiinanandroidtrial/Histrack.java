package com.gudugudi.gudugudiinanandroidtrial;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Histrack {
    @SerializedName("alarmType")
    @Expose
    private List<String> alarmType = null;
    @SerializedName("date")
    @Expose
    private List<Integer> date = null;
    @SerializedName("hour")
    @Expose
    private List<Object> hour = null;
    @SerializedName("latatitude")
    @Expose
    private List<Object> latatitude = null;
    @SerializedName("longitude")
    @Expose
    private List<Object> longitude = null;
    @SerializedName("minute")
    @Expose
    private List<Object> minute = null;
    @SerializedName("month")
    @Expose
    private List<Integer> month = null;
    @SerializedName("seconds")
    @Expose
    private List<Object> seconds = null;
    @SerializedName("speed")
    @Expose
    private List<Object> speed = null;
    @SerializedName("year")
    @Expose
    private List<Integer> year = null;
    public List<String> getAlarmType() {
        return alarmType;
    }

    public void setAlarmType(List<String> alarmType) {
        this.alarmType = alarmType;
    }

    public List<Integer> getDate() {
        return date;
    }

    public void setDate(List<Integer> date) {
        this.date = date;
    }

    public List<Object> getHour() {
        return hour;
    }

    public void setHour(List<Object> hour) {
        this.hour = hour;
    }

    public List<Object> getLatatitude() {
        return latatitude;
    }

    public void setLatatitude(List<Object> latatitude) {
        this.latatitude = latatitude;
    }

    public List<Object> getLongitude() {
        return longitude;
    }

    public void setLongitude(List<Object> longitude) {
        this.longitude = longitude;
    }

    public List<Object> getMinute() {
        return minute;
    }

    public void setMinute(List<Object> minute) {
        this.minute = minute;
    }

    public List<Integer> getMonth() {
        return month;
    }

    public void setMonth(List<Integer> month) {
        this.month = month;
    }

    public List<Object> getSeconds() {
        return seconds;
    }

    public void setSeconds(List<Object> seconds) {
        this.seconds = seconds;
    }

    public List<Object> getSpeed() {
        return speed;
    }

    public void setSpeed(List<Object> speed) {
        this.speed = speed;
    }

    public List<Integer> getYear() {
        return year;
    }

    public void setYear(List<Integer> year) {
        this.year = year;
    }

}