package com.gudugudi.gudugudiinanandroidtrial;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;

import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.SphericalUtil;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class IgnitionReport extends MainActivity  {
    Button click;
    public static TextView data;

    GoogleMap mMap;
    private LatLng sydney1;
    private LatLng BANGALORE;
    private int iconRadiusPixels;
    private  JsonIgnitionRecord jsonTrackRcrd;
    Polyline poly;
    int numberVchicles;
    String ImeiNumber;
   // Polyline poly;
    String IMEiArray[];
    String vehicleNumber[];
    String RecDate[];
    String RecMonth[];
    String RecYear[];
    String DHour[];
    String DMinute[];
    String DSecond[];
    String IgnitnState[];
    int currentIndexSet;
    int vehicleIndexSet;
    String MonthSet[] = {"jan","Jan","Feb","Mar","Apr","May","Jun",
            "Jul","Aug","Sep","Oct","Nov","Dec"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ignition_list);


        IMEiArray = getIntent().getStringArrayExtra("IMEINumberArray");
        vehicleNumber = getIntent().getStringArrayExtra("VehicleNumbers");
        vehicleIndexSet = 0;
        String URL = "hi";
        new DownloadXML1().execute(URL);

        numberLiners();


        }

    private class DownloadXML1 extends AsyncTask<String, Void, Void> {

        private Object view;

        @Override

        protected Void doInBackground(String... Url) {



            int arrayIndex = 0;

            String connstr = " https://www.gudugudi.com/and_getIgnDates.php";
            String result = "";
            int i = 0;
            try {

                result = "";
                URL url = new URL(connstr);
                HttpURLConnection http = (HttpURLConnection) url.openConnection();
                http.setRequestMethod("POST");
                http.setDoInput(true);
                http.setDoOutput(true);

                OutputStream ops = http.getOutputStream();

                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(ops, "UTF-8"));
                String data = URLEncoder.encode("imei", "UTF-8") + "=" + URLEncoder.encode(IMEiArray[vehicleIndexSet], "UTF-8");
                writer.write(data);
                writer.flush();
                writer.close();
                ops.close();

                InputStream ips = http.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(ips, "ISO-8859-1"));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    result += line;
                }
                if(result.length() < 1){
                    return  null;
                }
                Log.d("her gos", result);
                String[] strings = result.split(";");
                RecYear = strings[0].split(",");
                RecMonth = strings[1].split(",");
                RecDate = strings[2].split(",");



                Log.d("her gos", RecDate[0]);



            } catch (MalformedURLException e) {
                result = e.getMessage();
            } catch (IOException e) {
                result = e.getMessage();
            }
            return null;

        }


        protected void onPostExecute(Void args) {
            currentIndexSet = RecDate.length-1;
            Log.d("After execution", RecDate[0]);
           createLeftSlideDates();
            String URL = "hi";
            new DownloadHisData().execute("0");

            //  stUpMaps();
        }
    }

    private class DownloadHisData extends AsyncTask<String, Void, Void> {

        private Object view;

        @Override

        protected Void doInBackground(String... Url) {




            int arrayIndex = 0;
            //get number feild
            // for (int i = 0; i < trucKame.getLength(); i++) {
            // for (int i = 0; i < VechicleNumbers.length; i++) {

            String connstr = " https://www.gudugudi.com/and_getIgnDataByImei.php";
            String result = "";
            int i = 0;
            try {

                result = "";
                URL url = new URL(connstr);
                HttpURLConnection http = (HttpURLConnection) url.openConnection();
                http.setRequestMethod("POST");
                http.setDoInput(true);
                http.setDoOutput(true);

                OutputStream ops = http.getOutputStream();

                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(ops, "UTF-8"));
                String data = URLEncoder.encode("imei", "UTF-8") + "=" + URLEncoder.encode(IMEiArray[vehicleIndexSet], "UTF-8")
                        +"&&"+URLEncoder.encode("date","UTF-8")+"="+URLEncoder.encode(RecDate[currentIndexSet],"UTF-8")
                        +"&&"+URLEncoder.encode("month","UTF-8")+"="+URLEncoder.encode(RecMonth[currentIndexSet],"UTF-8")
                        +"&&"+URLEncoder.encode("year","UTF-8")+"="+URLEncoder.encode(RecYear[currentIndexSet],"UTF-8");
                writer.write(data);
                writer.flush();
                writer.close();
                ops.close();

                InputStream ips = http.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(ips, "ISO-8859-1"));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    result += line;
                }
                if(result.length() < 1){
                    return  null;
                }
                Log.d("Now Histrack data", result);
                String[] strings = result.split(";");
               // Latitude = strings[0].split(",");
               // Lanitude= strings[1].split(",");
               // Speed = strings[2].split(",");
                IgnitnState = strings[0].split(",");
                DHour = strings[1].split(",");
                DMinute = strings[2].split(",");
                DSecond = strings[3].split(",");
                // Log.d("her gos", RecDate[0]);



            } catch (MalformedURLException e) {
                result = e.getMessage();
            } catch (IOException e) {
                result = e.getMessage();
            }
            return null;

        }


        protected void onPostExecute(Void args) {
            ignitionList();

        }
    }

    protected  void numberLiners() {



        final LinearLayout lm = (LinearLayout) findViewById(R.id.numliners);


        for ( int i = 0 ; i < vehicleNumber.length ; i++) {


            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            LinearLayout lnrLay = new LinearLayout(this);
            //   Create LinearLayout

            RelativeLayout ll = new RelativeLayout(this);

            //  ll.setOrientation(RelativeLayout.HORIZONTAL);
            // Create TextView
            Button btn = new Button(this);
            // Give button an ID
            btn.setId(i);
            btn.setText(vehicleNumber[i]);

            btn.setLayoutParams(params);
            if(i != 0)
                btn.setBackgroundResource(R.drawable.roundedbutton1);
            else
                btn.setBackgroundResource(R.drawable.roundbuttongh);
            ((btn)).setTextColor(Color.parseColor("#ffffff"));
            final int index = i;




            lm.addView(btn);


            btn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    final LinearLayout lm = (LinearLayout) findViewById(R.id.liners);
                    lm.removeAllViews();
                    String URL = "hi";
                    int preInd = vehicleIndexSet;

                    vehicleIndexSet = v.getId();
                    currentIndexSet = 0;
                    Button  button = (Button) findViewById(preInd);// datGbV = v;
                    button.setBackgroundResource(R.drawable.roundedbutton1);
                    v.setBackgroundResource(R.drawable.roundbuttongh);
                    new DownloadXML1().execute(URL);

                }
            });

        }
    }

    protected void ignitionList() {

        final LinearLayout lm = (LinearLayout) findViewById(R.id.zone_list);

        lm.removeAllViews();

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);




            LinearLayout lnrLay = new LinearLayout(this);
            //   Create LinearLayout

            RelativeLayout ll = new RelativeLayout(this);



            // ll.setOrientation(RelativeLayout.HORIZONTAL);
            // Create TextView
            Button btn = new Button(this);
            // Give button an ID
            btn.setId(0);
          Button btns = (Button) findViewById(R.id.head);

          btns.setText( "Ignition State" + " | " + " At" + " | " + "Till" );
           // btn.setWidth((lm.getWidth() * 35* 4) / 100);
            // set the layoutParams on the button
            //params.setMargins(60, 0, 0, 0);
           // btn.setLayoutParams(params);
            Display display = ((WindowManager)getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
            int width = display.getWidth();
            int height = display.getHeight();
            btn.setLayoutParams(new LinearLayout.LayoutParams((int) (width/(1.35)), height/10));
            Log.e("(width/(1.5))", "ignitionList: "+(int)(width/(1.35)) );
            btn.setLayoutParams(params);
            btn.setBackgroundResource(R.drawable.roundedbutton1);
            ((btn)).setTextColor(Color.parseColor("#ffffff"));
            lm.addView(btn);

            int hrsStrt, minStrt , secStrt;
            String igState = IgnitnState[0].toString();
            int hrsEnd = Integer.parseInt(DHour[0].toString());
            int minEnd = Integer.parseInt(DMinute[0].toString());
            int secEnd = Integer.parseInt(DSecond[0].toString());

             hrsStrt = Integer.parseInt(DHour[0].toString());
             minStrt = Integer.parseInt(DMinute[0].toString());
             secStrt = Integer.parseInt(DSecond[0].toString());

             int preIndx = 0;

            for ( int i = 0 ; i < IgnitnState.length  ; i++) {


                LinearLayout.LayoutParams paramsa = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);


                if(igState.equals( IgnitnState[i].toString()) == false) {


                    Button btna = new Button(this);
                    // Give button an ID
                    btna.setId(i);
                    hrsStrt = Integer.parseInt(DHour[preIndx].toString());
                    minStrt = Integer.parseInt(DMinute[preIndx].toString());
                    secStrt = Integer.parseInt(DSecond[preIndx].toString());
                    hrsEnd = Integer.parseInt(DHour[i].toString());
                    minEnd = Integer.parseInt(DMinute[i].toString());
                    secEnd = Integer.parseInt(DSecond[i].toString());

                   int  strtVal = hrsStrt * 3600 + minStrt * 60;
                  int  endVal  =   hrsEnd * 3600 + minEnd * 60;
                    strtVal =   endVal - strtVal;
                  int   tymHr = strtVal / 3600;
                    strtVal = strtVal % 3600;
                    int   tymHrMIn = strtVal / 60;
                    if(igState.equals("OFF") == true) {

                        btna.setText(igState + "|" +
                                hrsStrt + ":" + minStrt + ":" + secStrt + " | " +
                                hrsEnd + ":" + minEnd + ":" + secEnd + " | " + "\n" +
                                "Off For--->  " + tymHr + " Hrs " + " And " + tymHrMIn + " Mins "
                        );
                    }else
                        btna.setText(igState + "|" +
                                hrsStrt + ":" + minStrt + ":" + secStrt + " | " +
                                hrsEnd + ":" + minEnd + ":" + secEnd + " | " + "\n" +
                                "On For--->  " + tymHr + " Hrs " + " And " + tymHrMIn + " Mins "
                        );

                    lm.addView(btna);
                    btna.setBackgroundResource(R.drawable.roundedbutton1);
                    ((btna)).setTextColor(Color.parseColor("#ffffff"));
                    //paramsa.setMargins(50, 0, 0, 0);
                    btna.setLayoutParams(paramsa);

                    igState = IgnitnState[i].toString();
                    preIndx = i;
                }


                if(i == IgnitnState.length-1 && preIndx == 0) {

                    Button btna = new Button(this);
                    // Give button an ID
                    btna.setId(i);
                    hrsStrt = Integer.parseInt(DHour[0].toString());
                    minStrt = Integer.parseInt(DMinute[0].toString());
                    secStrt = Integer.parseInt(DSecond[0].toString());
                    hrsEnd = Integer.parseInt(DHour[IgnitnState.length-1].toString());
                    minEnd = Integer.parseInt(DMinute[IgnitnState.length-1].toString());
                    secEnd = Integer.parseInt(DSecond[IgnitnState.length-1].toString());


                    int  strtVal = hrsStrt * 3600 + minStrt * 60;
                    int  endVal  =   hrsEnd * 3600 + minEnd * 60;
                    strtVal =   endVal - strtVal;
                    int   tymHr = strtVal / 3600;
                    strtVal = strtVal % 3600;
                    int   tymHrMIn = strtVal / 60;

                    igState = IgnitnState[0].toString();
                    if(igState.equals("OFF") == true) {

                        btna.setText(igState + "|" +
                                hrsStrt + ":" + minStrt + ":" + secStrt + " | " +
                                hrsEnd + ":" + minEnd + ":" + secEnd + " | " + "\n" +
                                "Off For--->  " + tymHr + " Hrs " + " And " + tymHrMIn + " Mins "
                        );
                    }else
                        btna.setText(igState + "|" +
                                hrsStrt + ":" + minStrt + ":" + secStrt + " | " +
                                hrsEnd + ":" + minEnd + ":" + secEnd + " | " + "\n" +
                                "On For--->  " + tymHr + " Hrs " + " And " + tymHrMIn + " Mins "
                        );



                    lm.addView(btna);
                    btna.setBackgroundResource(R.drawable.roundedbutton1);
                    ((btna)).setTextColor(Color.parseColor("#ffffff"));
                    //paramsa.setMargins(50, 0, 0, 0);
                    btna.setLayoutParams(paramsa);


                }





            }


    }


    protected void createLeftSlideDates() {

        final LinearLayout lm = (LinearLayout) findViewById(R.id.liners);

        for ( int i = RecDate.length-1 ; i >= 0 ; i--) {


            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);




            LinearLayout lnrLay = new LinearLayout(this);
            //   Create LinearLayout

            RelativeLayout ll = new RelativeLayout(this);

            // ll.setOrientation(RelativeLayout.HORIZONTAL);
            // Create TextView
            Button btn = new Button(this);
            // Give button an ID
            btn.setId(i);

            String dt = RecDate[i] + "-" +
                    MonthSet[Integer.parseInt(RecMonth[i].toString())] + "-" +RecYear[i];//(RecDate[i] + "-" +RecMonth[i] + "-" +RecYear[i]);

            // set the layoutParams on the button
            btn.setText(dt);
            btn.setLayoutParams(params);
            if(RecDate.length-1 != i)
                btn.setBackgroundResource(R.drawable.roundedbutton1);
            else
                btn.setBackgroundResource(R.drawable.roundbuttongh);
            ((btn)).setTextColor(Color.parseColor("#ffffff"));
            // btn.setShadowLayer(5, 0, 0, Color.WHITE);
            final int index = i;



            lm.addView(btn);

            btn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    int preInd = currentIndexSet;
                    currentIndexSet = v.getId();

                    Button  button = (Button) findViewById(preInd);// datGbV = v;
                    button.setBackgroundResource(R.drawable.roundedbutton1);

                    v.setBackgroundResource(R.drawable.roundbuttongh);
                    String URL = "hi";
                    new DownloadHisData().execute("0");


                }

            });

        }


    }


}
