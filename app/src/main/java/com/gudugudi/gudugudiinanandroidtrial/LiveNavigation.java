package com.gudugudi.gudugudiinanandroidtrial;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.SphericalUtil;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Locale;

public class LiveNavigation  extends MainActivity  implements OnMapReadyCallback, GoogleMap.OnCameraMoveStartedListener, GoogleMap.OnCameraIdleListener {
    Button click;
    public static TextView data;

    GoogleMap mMap;
    private LatLng sydney1;
    private LatLng BANGALORE;
    private int iconRadiusPixels;
    int threadReles = 0;
    private JsonTrackRecord jsonTrackRcrd;
    int numberVchicles;
    String ImeiNumber;
    Polyline poly;
    String IMEiArray[];
    String vehicleNumber[];
    String RecDate[];
    String RecMonth[];
    String RecYear[];
    String Latitude[];
    String Lanitude[];
    String Speed[];
    String DHour[];
    String DMinute[];
    String DSecond[];
    String Ignition[];
    int currentIndexSet;
    int vehicleIndexSet;
    int latIndx;
    int exit_pressed;
    int clked_vehicleIndex;
    int loadx;
    int loadlength;
    int loadFailed;
    Handler handler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_livenavigation);


        IMEiArray = getIntent().getStringArrayExtra("IMEINumberArray");
        vehicleNumber = getIntent().getStringArrayExtra("VehicleNumbers");
        vehicleIndexSet = 0;
        latIndx = 0;
        String URL = "hi";
        exit_pressed = 0;
        new DownloadXML1().execute(URL);

        numberLiners();
    }


    private class DownloadXML1 extends AsyncTask<String, Void, Void> {

        private Object view;

        @Override

        protected Void doInBackground(String... Url) {

            loadFailed = 0;

            //String Ignition[];
            String connstr = " https://www.gudugudi.com/getLiveNaviValue.php";
            String result = "";
            int i = 0;
            try {

                result = "";
                URL url = new URL(connstr);
                HttpURLConnection http = (HttpURLConnection) url.openConnection();
                http.setRequestMethod("POST");
                http.setDoInput(true);
                http.setDoOutput(true);

                OutputStream ops = http.getOutputStream();

                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(ops, "UTF-8"));
                String data = URLEncoder.encode("imei", "UTF-8") + "=" + URLEncoder.encode(IMEiArray[vehicleIndexSet], "UTF-8");
                writer.write(data);
                writer.flush();
                writer.close();
                ops.close();

                InputStream ips = http.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(ips, "ISO-8859-1"));
                String line = "";

                while ((line = reader.readLine()) != null) {
                    result += line;
                }


                if(result.compareTo("Error") == 0)
                {
                    loadFailed = 0;
                    return  null;
                }

               loadFailed = 1;
                Log.d("Live Navi", result);

                String[] strings = result.split(";");


                 if(loadFailed != 0) {
                     Latitude = strings[0].split(",");
                     Lanitude = strings[1].split(",");
                     Speed = strings[2].split(",");
                     DHour = strings[3].split(",");
                     DMinute = strings[4].split(",");
                     DSecond = strings[5].split(",");
                 }

            } catch (MalformedURLException e) {
                result = e.getMessage();
            } catch (IOException e) {
                result = e.getMessage();
            }
            return null;


        }


        protected void onPostExecute(Void args) {




            // Showing Alert Message


                  if(loadFailed == 1);
                    stUpMaps();


        }
    }

    private class DownloadXML12 extends AsyncTask<String, Void, Void> {

        private Object view;

        @Override

        protected Void doInBackground(String... Url) {



            String connstr = " https://www.gudugudi.com/getLiveNaviValue.php";
            String result = "";
            int i = 0;
            try {

                result = "";
                URL url = new URL(connstr);
                HttpURLConnection http = (HttpURLConnection) url.openConnection();
                http.setRequestMethod("POST");
                http.setDoInput(true);
                http.setDoOutput(true);

                OutputStream ops = http.getOutputStream();

                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(ops, "UTF-8"));
                String data = URLEncoder.encode("imei", "UTF-8") + "=" + URLEncoder.encode(IMEiArray[vehicleIndexSet], "UTF-8");
                writer.write(data);
                writer.flush();
                writer.close();
                ops.close();

                InputStream ips = http.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(ips, "ISO-8859-1"));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    result += line;
                }
                Log.d("Live Navi", result);


                String[] strings = result.split(";");


                Latitude = strings[0].split(",");
                Lanitude = strings[1].split(",");
                Speed = strings[2].split(",");
                DHour = strings[3].split(",");
                DMinute = strings[4].split(",");
                DSecond = strings[5].split(",");

                // move_withLats();

            } catch (MalformedURLException e) {
                result = e.getMessage();
            } catch (IOException e) {
                result = e.getMessage();
            }
            return null;


        }


        protected void onPostExecute(Void args) {


            move_withLats();
        }
    }

    public void move_withLats() {
        int height = 55;
        int width = 35;
        String address;// = addresses.get(0).getAddressLine(0);
        String city; //= addresses.get(0).getAddressLine(1);
        String country; //= addresses.get(0).getAddressLine(2);
        if(loadFailed == 1) {
            mMap.clear();
            try {
                //poly.remove();
                // Add a marker in Sydney and move the camera
                //
                sydney1 = new LatLng(Double.parseDouble(Latitude[Latitude.length - 1].toString()),
                        Double.parseDouble(Lanitude[Latitude.length - 1].toString()));
                Geocoder geoCoder = new Geocoder(getApplicationContext(),
                        Locale.getDefault());
                List<Address> addresses;


                addresses = geoCoder.getFromLocation(Double.parseDouble(Latitude[Latitude.length - 1].toString()),
                        Double.parseDouble(Lanitude[Lanitude.length - 1].toString()), 1);

                address = addresses.get(0).getAddressLine(0);
                city = addresses.get(0).getAddressLine(1);
                country = addresses.get(0).getAddressLine(2);

                BitmapDrawable bitmapdrawstart = (BitmapDrawable) getResources().getDrawable(R.drawable.truck);
                BitmapDrawable bitmapdrawend = (BitmapDrawable) getResources().getDrawable(R.drawable.truck);
                Bitmap b1 = getBitmap(this, R.drawable.carimg);//bitmapdrawstart.getBitmap();
                Bitmap b2 = getBitmap(this, R.drawable.carimg);
                Bitmap smallMarkerstart = Bitmap.createScaledBitmap(b1, width, height, false);
                Bitmap smallMarkerend = Bitmap.createScaledBitmap(b2, width, height, false);

                if (Latitude.length > 1) {
                    float xn, yn, test = 180;
                    if(GetBearing(new LatLng(Double.parseDouble(Latitude[Latitude.length - 2].toString()),
                            Double.parseDouble(Lanitude[Latitude.length - 2].toString())),new LatLng(Double.parseDouble(Latitude[Latitude.length - 1].toString()),
                            Double.parseDouble(Lanitude[Latitude.length - 1].toString()))) < 90) {
                        xn = 0f;
                        yn = 3f;
                    }
                    else if(GetBearing(new LatLng(Double.parseDouble(Latitude[Latitude.length - 2].toString()),
                            Double.parseDouble(Lanitude[Latitude.length - 2].toString())),new LatLng(Double.parseDouble(Latitude[Latitude.length - 1].toString()),
                            Double.parseDouble(Lanitude[Latitude.length - 1].toString()))) < 180) {

                        ;            xn = 3f;
                        yn = 0f;
                    } else if(GetBearing(new LatLng(Double.parseDouble(Latitude[Latitude.length - 2].toString()),
                            Double.parseDouble(Lanitude[Latitude.length - 2].toString())),new LatLng(Double.parseDouble(Latitude[Latitude.length - 1].toString()),
                            Double.parseDouble(Lanitude[Latitude.length - 1].toString())))  < 270) {

                        xn = -3f;
                        yn = 0f;
                    }
                    else
                    {
                        xn = 0f;
                        yn = -3f;
                    }
                    double angle = 130.0; // rotation angle
                    double x = Math.sin(-angle * Math.PI / 180) * 0.8 + 0.8;
                    double y = -(Math.cos(-angle * Math.PI / 180) * 0.2 - 0.2);

                    mMap.addMarker(new MarkerOptions().position(sydney1).infoWindowAnchor((float)x, (float)y).anchor(0.5f, 0.5f).rotation(GetBearing(new LatLng(Double.parseDouble(Latitude[Latitude.length - 2].toString()),
                            Double.parseDouble(Lanitude[Latitude.length - 2].toString())), new LatLng(Double.parseDouble(Latitude[Latitude.length - 1].toString()),
                            Double.parseDouble(Lanitude[Latitude.length - 1].toString()))))
                          .draggable(false).visible(true).title("Address--" + address + "\n" + city + "\n" + country + "\n"
                            ).
                                    snippet(String.valueOf(
                                            "Speed--->" + Speed[Speed.length - 1] + "     Reached At-->" + DHour[DHour.length - 1] + ":" + DMinute[DHour.length - 1])).visible(true)
                            .icon(BitmapDescriptorFactory.fromBitmap(smallMarkerend))).showInfoWindow();
                }
                else
                    mMap.addMarker(new MarkerOptions().position(sydney1).infoWindowAnchor(1f, 1f).anchor(0.5f, 0.5f)
                            .draggable(false).visible(true).title("Address--" + address + "\n" + city + "\n" + country + "\n"
                            ).
                                    snippet(String.valueOf(
                                            "Speed--->" + Speed[Speed.length - 1] + "     Reached At-->" + DHour[DHour.length - 1] + ":" + DMinute[DHour.length - 1])).visible(true)
                            .icon(BitmapDescriptorFactory.fromBitmap(smallMarkerend))).showInfoWindow();

                // mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney1, 18F));
                //this.showCustomPolyline();


                if (loadlength > Lanitude.length && Lanitude.length > 1) {
                    CameraPosition position = CameraPosition.builder()
                            .target(sydney1)
                            .zoom(15F)
                            .tilt(30)
                            .build();
                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), 1000, new GoogleMap.CancelableCallback() {
                        @Override
                        public void onFinish() {
                            //Here you can take the snapshot or whatever you want
                            String URl = "";
                            new DownloadXML12().execute(URl);

                        }


                        @Override
                        public void onCancel() {

                        }
                    });
                } else if (Lanitude.length == 1) {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney1, 15F));
                    handler.postDelayed(timedTask, 10000);
                } else {
                    //String URl = "";
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney1, 15F));
                    String URl = "";
                    handler.postDelayed(timedTask, 10000);
                }
                latIndx++;
            } catch (IOException e) {
                e.printStackTrace();
            }

            loadlength = Lanitude.length;
        }


    }
    Runnable timedTask = new Runnable(){

        @Override
        public void run() {

            String URl = "";
            new DownloadXML12().execute(URl);
        }};

    private final double degreesPerRadian = 180.0 / Math.PI;

    private float GetBearing(LatLng from, LatLng to){
        double lat1 = from.latitude * Math.PI / 180.0;
        double lon1 = from.longitude * Math.PI / 180.0;
        double lat2 = to.latitude * Math.PI / 180.0;
        double lon2 = to.longitude * Math.PI / 180.0;

        // Compute the angle.
        double angle = - Math.atan2( Math.sin( lon1 - lon2 ) * Math.cos( lat2 ), Math.cos( lat1 ) * Math.sin( lat2 ) - Math.sin( lat1 ) * Math.cos( lat2 ) * Math.cos( lon1 - lon2 ) );

        if (angle < 0.0)
            angle += Math.PI * 2.0;

        // And convert result to degrees.
        angle = angle * degreesPerRadian;

        return (float) angle;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {



            mMap = googleMap;
            String address;// = addresses.get(0).getAddressLine(0);
            String city; //= addresses.get(0).getAddressLine(1);
            String country; //= addresses.get(0).getAddressLine(2);

            mMap.setOnCameraMoveStartedListener(this);
            mMap.setOnCameraIdleListener(this);

            mMap.getUiSettings().setZoomControlsEnabled(true);


            int inx = 0;


            int height = 55;
            int width = 35;
         if(loadFailed == 1) {
            //  this.showCustomPolyline();
            // Add a marker in Sydney and move the camera
            sydney1 = new LatLng(Double.parseDouble(Latitude[Latitude.length - 1].toString()),
                    Double.parseDouble(Lanitude[Lanitude.length - 1].toString()));
//        BANGALORE =new LatLng(Double.parseDouble(Latitude[Latitude.length-1].toString()),
//                Double.parseDouble(Lanitude[Latitude.length-1].toString()));

            // BitmapDescriptor iconDescr = BitmapDescriptorFactory.fromResource(R.drawable.truck);


            BitmapDrawable bitmapdrawstart = (BitmapDrawable) getResources().getDrawable(R.drawable.truck);
            BitmapDrawable bitmapdrawend = (BitmapDrawable) getResources().getDrawable(R.drawable.truck);
            Bitmap b1 = bitmapdrawstart.getBitmap();
            Bitmap b2 = getBitmap(this, R.drawable.carimg);
            Bitmap smallMarkerstart = Bitmap.createScaledBitmap(b1, width, height, false);
            Bitmap smallMarkerend = Bitmap.createScaledBitmap(b2, width, height, false);

            try {
                Geocoder geoCoder = new Geocoder(getApplicationContext(),
                        Locale.getDefault());


                List<Address> addresses;


                addresses = geoCoder.getFromLocation(Double.parseDouble(Latitude[Latitude.length - 1].toString()),
                        Double.parseDouble(Lanitude[Lanitude.length - 1].toString()), 1);

                address = addresses.get(0).getAddressLine(0);
                city = addresses.get(0).getAddressLine(1);
                country = addresses.get(0).getAddressLine(2);

                mMap.addMarker(new MarkerOptions().position(sydney1).anchor(0.5F, 0.5F)
                        .draggable(false).visible(true).title("Present").visible(true)
                        .icon(BitmapDescriptorFactory.fromBitmap(smallMarkerend))).showInfoWindow();

            } catch (IOException e) {
                e.printStackTrace();
            }


            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney1, 18F));
            latIndx++;
            loadx = Lanitude.length - 1;
            loadlength = loadx;

            String URl = "";
            new DownloadXML12().execute(URl);
        }

    }

    @Override
    public void onCameraMoveStarted(int reason) {

    }

    @Override
    public void onCameraIdle() {

    }

    public double totalDistanceTravalled() {

        int i = 0;
        double distance = 0;
        while (i < Latitude.length - 2) {

            distance = distance + setDistance(i);
            i++;
        }

        return distance;

    }

    public double setDistance(int i) {

        return lngDistance(Double.parseDouble(Latitude[i].toString()), Double.parseDouble(Lanitude[i].toString())
                , Double.parseDouble(Latitude[i + 1].toString()),
                Double.parseDouble(Lanitude[i + 1].toString()));
    }

    public double lngDistance(double lat1, double lon1, double lat2, double lon2) {
        int R = 6371; // km (change this constant to get miles)
        double dLat = (lat2 - lat1) * Math.PI / 180;
        double dLon = (lon2 - lon1) * Math.PI / 180;
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
                        Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = R * c;
    /*	if (d>1) {
               return d; //+"km";
         }
    	else if (d<=1) {
           return d;/// +"m";
       } */
        return d;
    }


    private void showCustomPolyline() {


        sydney1 = new LatLng(Double.parseDouble(Latitude[loadx].toString()),
                Double.parseDouble(Lanitude[loadx].toString()));
        BANGALORE = new LatLng(Double.parseDouble(Latitude[Latitude.length - 1].toString()),
                Double.parseDouble(Lanitude[Latitude.length - 1].toString()));


        //Get projection
        Projection proj = mMap.getProjection();
        //Get a point on the screen that corresponds to first marker
        Point p = proj.toScreenLocation(BANGALORE);

        //Lets create another point that is shifted on number of pixels iqual to icon radius
        //This point is located on circle border
        Point b = new Point(p.x + iconRadiusPixels, p.y);
        //Get the LatLng for a point on the circle border
        LatLng l = proj.fromScreenLocation(b);
        //Calculate the radius of the icon (distance between center and point on the circle border)
        double r = SphericalUtil.computeDistanceBetween(sydney1, l);
        //Calculate heading from point 1 to point 2 and from point 2 to point 1
        double heading1 = SphericalUtil.computeHeading(sydney1, BANGALORE);
        double heading2 = SphericalUtil.computeHeading(BANGALORE, sydney1);


        //Create polyline
        PolylineOptions options = new PolylineOptions();

        for (int i = loadx; i < Latitude.length; i++) {

            LatLng pos1 = new LatLng(Double.parseDouble(Latitude[i].toString()),
                    Double.parseDouble(Lanitude[i].toString()));
            options.add(pos1);
        }

        poly = mMap.addPolyline(options.width(8).color(Color.RED).geodesic(false));


        latIndx++;

        String URL = "hi";


    }

    protected void numberLiners() {


        final LinearLayout lm = (LinearLayout) findViewById(R.id.numliners);


        for (int i = 0; i < vehicleNumber.length; i++) {


            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);


            //      params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

            // create the layout params that will be used to define how your
            // button will be displayed

            LinearLayout lnrLay = new LinearLayout(this);
            //   Create LinearLayout

            RelativeLayout ll = new RelativeLayout(this);

            //  ll.setOrientation(RelativeLayout.HORIZONTAL);
            // Create TextView
            Button btn = new Button(this);
            // Give button an ID
            btn.setId(i);
            btn.setText(vehicleNumber[i]);
            //   btn.setWidth((lm.getWidth() * 35* 4) / 100);
            // set the layoutParams on the button
            btn.setLayoutParams(params);

            if(i != 0)
                btn.setBackgroundResource(R.drawable.roundedbutton1);
            else
                btn.setBackgroundResource(R.drawable.roundbuttongh);
            ((btn)).setTextColor(Color.parseColor("#ffffff"));
            // btn.setShadowLayer(3, 0, 0, Color.WHITE);
            //btn.setAlpha(0.3f);
            final int index = i;


            //Add button to LinearLayout defined in XML
            lm.addView(btn);


            btn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if(loadFailed == 1)
                       mMap.clear();
                   // clked_vehicleIndex = v.getId();
                    ///       latIndx = 0;
                    int preInd = vehicleIndexSet;
                    String URL = "hi";
                      vehicleIndexSet = v.getId();
                    currentIndexSet = 0;
                    loadx = 0;
                    loadlength = 0;
                    loadFailed = 0;
                    Button button = (Button) findViewById(preInd);// datGbV = v;
                    button.setBackgroundResource(R.drawable.roundedbutton1);
                    v.setBackgroundResource(R.drawable.roundbuttongh);
                   new DownloadXML1().execute(URL);
                   // exit_pressed = 1;
                }
            });

        }
    }


    protected void stUpMaps() {
        //      //   Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.main_activity_map);


        //Let's calculate what is a radius in screen pixels for our icon
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        Drawable m_icon = ResourcesCompat.getDrawableForDensity(getResources(), R.drawable.truck, dm.densityDpi, null);
        int height = ((BitmapDrawable) m_icon).getBitmap().getHeight();
        int width = ((BitmapDrawable) m_icon).getBitmap().getWidth();

        iconRadiusPixels = width / 2;
        mapFragment.getMapAsync(this);
    }

    public Bitmap getBitmap(VectorDrawable vectorDrawable) {
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        vectorDrawable.draw(canvas);
        return bitmap;
    }

    public Bitmap getBitmap(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (drawable instanceof BitmapDrawable) {
            return BitmapFactory.decodeResource(context.getResources(), drawableId);
        } else if (drawable instanceof VectorDrawable) {
            return getBitmap((VectorDrawable) drawable);
        } else {
            throw new IllegalArgumentException("unsupported drawable type");
        }

    }
}
