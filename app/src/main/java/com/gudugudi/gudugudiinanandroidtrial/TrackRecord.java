package com.gudugudi.gudugudiinanandroidtrial;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.StrictMode;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.SignInButton;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;

import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.RoundCap;
import com.google.maps.android.SphericalUtil;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimerTask;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

public class TrackRecord extends MainActivity  implements OnMapReadyCallback, GoogleMap.OnCameraMoveStartedListener, GoogleMap.OnCameraIdleListener {
    Button click;
    public static TextView data;

    GoogleMap mMap;
    private LatLng sydney1;
    private LatLng BANGALORE;
    private int iconRadiusPixels;
    int threadReles = 0;
    private  JsonTrackRecord jsonTrackRcrd;
    int numberVchicles;
    String ImeiNumber;
    Polyline poly;
    String[] IMEiArray;
    String[] vehicleNumber;
    String[] RecDate;
    String[] RecMonth;
    String[] RecYear;
    String[] Latitude;
    String[] Lanitude;
    String[] Speed;
    String[] DHour;
    String[] DMinute;
    String[] DSecond;
    int currentIndexSet;
    int vehicleIndexSet;
    View datGbV;
    int firstTymDateClick;
    String[] MonthSet = {"jan", "Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);


        IMEiArray = getIntent().getStringArrayExtra("IMEINumberArray");
        vehicleNumber = getIntent().getStringArrayExtra("VehicleNumbers");
        vehicleIndexSet = 0;
        currentIndexSet = 0;
        firstTymDateClick =0;
        String URL = "hi";
        new DownloadXML1().execute(URL);

            numberLiners();
    }



    private class DownloadXML1 extends AsyncTask<String, Void, Void> {

        private Object view;

        @Override

        protected Void doInBackground(String... Url) {



            int arrayIndex = 0;

            String connstr = " https://www.gudugudi.com/and_getHisDates.php";
            String result = "";
            int i = 0;
            try {

                    result = "";
                    URL url = new URL(connstr);
                    HttpURLConnection http = (HttpURLConnection) url.openConnection();
                    http.setRequestMethod("POST");
                    http.setDoInput(true);
                    http.setDoOutput(true);

                    OutputStream ops = http.getOutputStream();

                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(ops, StandardCharsets.UTF_8));
                    String data = URLEncoder.encode("imei", "UTF-8") + "=" + URLEncoder.encode(IMEiArray[vehicleIndexSet], "UTF-8");
                    writer.write(data);
                    writer.flush();
                    writer.close();
                    ops.close();

                    InputStream ips = http.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(ips, StandardCharsets.ISO_8859_1));
                    String line = "";
                    while ((line = reader.readLine()) != null) {
                        result += line;
                    }
                if(result.length() < 1){
                    return  null;
                }
                    Log.d("her gos", result);
                    String[] strings = result.split(";");
                    RecYear = strings[0].split(",");
                    RecMonth = strings[1].split(",");
                    RecDate = strings[2].split(",");



                Log.d("her gos", RecDate[0]);



            } catch (MalformedURLException e) {
                result = e.getMessage();
            } catch (IOException e) {
                result = e.getMessage();
            }
            return null;

        }


        protected void onPostExecute(Void args) {

            Log.d("After execution", RecDate[0]);
            currentIndexSet = RecDate.length-1;
            createLeftSlideDates();

            firstTymDateClick =0;
            String URL = "hi";
            new DownloadHisData().execute("0");

        }
    }
    private class DownloadHisData extends AsyncTask<String, Void, Void> {

        private Object view;

        @Override

        protected Void doInBackground(String... Url) {




            int arrayIndex = 0;
            //get number feild
            // for (int i = 0; i < trucKame.getLength(); i++) {
            // for (int i = 0; i < VechicleNumbers.length; i++) {

            String connstr = " https://www.gudugudi.com/and_getTrckRecordByImei.php";
            String result = "";
            int i = 0;
            try {

                result = "";
                URL url = new URL(connstr);
                HttpURLConnection http = (HttpURLConnection) url.openConnection();
                http.setRequestMethod("POST");
                http.setDoInput(true);
                http.setDoOutput(true);

                OutputStream ops = http.getOutputStream();

                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(ops, StandardCharsets.UTF_8));
                String data = URLEncoder.encode("imei", "UTF-8") + "=" + URLEncoder.encode(IMEiArray[vehicleIndexSet], "UTF-8")
                        +"&&"+URLEncoder.encode("date","UTF-8")+"="+URLEncoder.encode(RecDate[currentIndexSet],"UTF-8")
                        +"&&"+URLEncoder.encode("month","UTF-8")+"="+URLEncoder.encode(RecMonth[currentIndexSet],"UTF-8")
                        +"&&"+URLEncoder.encode("year","UTF-8")+"="+URLEncoder.encode(RecYear[currentIndexSet],"UTF-8");
                writer.write(data);
                writer.flush();
                writer.close();
                ops.close();

                InputStream ips = http.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(ips, StandardCharsets.ISO_8859_1));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    result += line;
                }
                if(result.length() < 1){
                    return  null;
                }
                Log.d("Now Histrack data", result);
                String[] strings = result.split(";");
                Latitude = strings[0].split(",");
                Lanitude= strings[1].split(",");
                Speed = strings[2].split(",");
                DHour = strings[3].split(",");
                DMinute = strings[4].split(",");
                DSecond = strings[5].split(",");
               // Log.d("her gos", RecDate[0]);



            } catch (MalformedURLException e) {
                result = e.getMessage();
            } catch (IOException e) {
                result = e.getMessage();
            }
            return null;

        }


        protected void onPostExecute(Void args) {
//            ProgressBar spinner;
//            spinner = (ProgressBar)findViewById(R.id.progressBar);
//
//
//            spinner.setVisibility(View.VISIBLE);

            stUpMaps();

//           spinner.setVisibility(View.GONE);

        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        String address;// = addresses.get(0).getAddressLine(0);
        String city; //= addresses.get(0).getAddressLine(1);
        String country; //= addresses.get(0).getAddressLine(2);

        mMap.setOnCameraMoveStartedListener(this);
        mMap.setOnCameraIdleListener(this);

        mMap.getUiSettings().setZoomControlsEnabled(true);





        int inx = 0;


            this.showCustomPolyline();
            // Add a marker in Sydney and move the camera
            sydney1 = new LatLng(Double.parseDouble(Latitude[0]),
                    Double.parseDouble(Lanitude[0]));
            BANGALORE =new LatLng(Double.parseDouble(Latitude[Latitude.length-1]),
                    Double.parseDouble(Lanitude[Latitude.length-1]));

            // BitmapDescriptor iconDescr = BitmapDescriptorFactory.fromResource(R.drawable.truck);

            int height = 50;
            int width = 50;

            BitmapDrawable bitmapdrawstart=(BitmapDrawable)getResources().getDrawable(R.drawable.start_s);
            BitmapDrawable bitmapdrawend=(BitmapDrawable)getResources().getDrawable(R.drawable.stop_s);
            Bitmap b1=bitmapdrawstart.getBitmap();
            Bitmap b2=bitmapdrawend.getBitmap();
            Bitmap smallMarkerstart = Bitmap.createScaledBitmap(b1, width, height, false);
            Bitmap smallMarkerend = Bitmap.createScaledBitmap(b2, width, height, false);

 try {
        Geocoder geoCoder = new Geocoder(getApplicationContext(),
                Locale.getDefault());
        // while(jsonTrackRcrd.allSets == 0){
        // ;
       // Log.e("latitude&longi", "onMapReady: "+latList[j] +" ." + langList[j]);
        //}


        List<Address> addresses;


     addresses = geoCoder.getFromLocation(Double.parseDouble(Latitude[0]),
             Double.parseDouble(Lanitude[0]), 1);

     address = addresses.get(0).getAddressLine(0);
     city = addresses.get(0).getAddressLine(1);
     country = addresses.get(0).getAddressLine(2);

     mMap.addMarker(new MarkerOptions().position(sydney1).anchor(0.5F, 0.5F)
             .draggable(false).visible(true).title("Start").
                     snippet("Address--" + address + "\n" + city + "\n" + country + "\n"
                             + "\n Reached At-->" + DHour[0] + ":" + DMinute[0])
             .icon(BitmapDescriptorFactory.fromBitmap(smallMarkerend))).showInfoWindow();

     addresses.clear();

     addresses = geoCoder.getFromLocation(Double.parseDouble(Latitude[Latitude.length-1]),
             Double.parseDouble(Lanitude[Latitude.length-1]), 1);

     address = addresses.get(0).getAddressLine(0);
     city = addresses.get(0).getAddressLine(1);
     country = addresses.get(0).getAddressLine(2);


     /*BitmapDescriptor iconDescr = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE);*/
     mMap.addMarker(new MarkerOptions().position(BANGALORE).anchor(0.5F, 0.5F).flat(false)
             .draggable(false).visible(true).title("End-->DistanceTravelled:-"+(int)totalDistanceTravalled()+"Kms").
                     snippet("Address--" + address + "\n" + city + "\n" + country + "\n"
                             + "\n Reached At-->" + DHour[DHour.length - 1] + ":" + DMinute[DHour.length - 1]).visible(true)
             .icon(BitmapDescriptorFactory.fromBitmap(smallMarkerstart))).showInfoWindow();

    } catch (IOException e) {
        e.printStackTrace();
    }





            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(BANGALORE, 10F));



    }

    @Override
    public void onCameraMoveStarted(int reason) {

    }

    @Override
    public void onCameraIdle() {


    }

public double totalDistanceTravalled() {

    int i = 0;
    double distance = 0 ;
    while( i < Latitude .length -2) {

        distance =  distance + setDistance(i);
        i++;
    }

    return distance;

}
    public double setDistance(int i) {

        return  lngDistance(Double.parseDouble(Latitude [i]),Double.parseDouble(Lanitude [i])
                , Double.parseDouble(Latitude [i + 1]),
                Double.parseDouble(Lanitude [i + 1]) );
    }
public double lngDistance(double lat1,double lon1,double lat2,double lon2) {
    int R = 6371; // km (change this constant to get miles)
    double dLat = (lat2-lat1) * Math.PI / 180;
    double dLon = (lon2-lon1) * Math.PI / 180;
    double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(lat1 * Math.PI / 180 ) * Math.cos(lat2 * Math.PI / 180 ) *
                    Math.sin(dLon/2) * Math.sin(dLon/2);
    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    double d = R * c;
    /*	if (d>1) {
               return d; //+"km";
         }
    	else if (d<=1) {
           return d;/// +"m";
       } */
    return d;
}



    private void showCustomPolyline() {


            sydney1 = new LatLng(Double.parseDouble(Latitude[0]),
                    Double.parseDouble(Lanitude[0]));
            BANGALORE =new LatLng(Double.parseDouble(Latitude[Latitude.length-1]),
                    Double.parseDouble(Lanitude[Latitude.length-1]));


            //Get projection
            Projection proj = mMap.getProjection();
            //Get a point on the screen that corresponds to first marker
            Point p = proj.toScreenLocation(BANGALORE);

            //Lets create another point that is shifted on number of pixels iqual to icon radius
            //This point is located on circle border
            Point b = new Point(p.x + iconRadiusPixels, p.y);
            //Get the LatLng for a point on the circle border
            LatLng l = proj.fromScreenLocation(b);
            //Calculate the radius of the icon (distance between center and point on the circle border)
            double r = SphericalUtil.computeDistanceBetween(sydney1, l);
            //Calculate heading from point 1 to point 2 and from point 2 to point 1
            double heading1 = SphericalUtil.computeHeading(sydney1, BANGALORE);
            double heading2 = SphericalUtil.computeHeading(BANGALORE, sydney1);


            //Create polyline
            PolylineOptions options = new PolylineOptions();
            //Calculate real position where the polyline starts and ends taking into account radius and heading
            //  LatLng pos1 = SphericalUtil.computeOffset(sydney1, r, heading1);
            //  LatLng pos2 = SphericalUtil.computeOffset(BANGALORE, r, heading2);

        int setId = 0;
            for(int i =0;  i < Latitude.length ; i++ ) {

                LatLng pos1 =  new LatLng(Double.parseDouble(Latitude[i]),
                        Double.parseDouble(Lanitude[i]));
                if(i < Latitude.length-1 ) {
                    LatLng pos2 = new LatLng(Double.parseDouble(Latitude[i + 1]),
                            Double.parseDouble(Lanitude[i + 1]));

                    int tsetTym = Integer.parseInt(DHour[setId])*60
                            + Integer.parseInt(DMinute[setId]) + 10;
                    int mTym = Integer.parseInt(DHour[i])*60
                            + Integer.parseInt(DMinute[i]);
                    if(mTym > tsetTym) {
                        setId = i;
                        DrawArrowHead(mMap, pos1, pos2);
                    }

                }

                options.add(pos1);
            }


            poly = mMap.addPolyline(options.width(8).color(Color.RED).geodesic(false));



    }


    private final double degreesPerRadian = 180.0 / Math.PI;

    private void DrawArrowHead(GoogleMap mMap, LatLng from, LatLng to){
        // obtain the bearing between the last two points
        double bearing = GetBearing(from, to);

        // round it to a multiple of 3 and cast out 120s
        double adjBearing = Math.round(bearing / 3) * 3;
        while (adjBearing >= 120) {
            adjBearing -= 120;
        }

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // Get the corresponding triangle marker from Google
        URL url;
        Bitmap image = null;

        try {
            url = new URL("http://www.google.com/intl/en_ALL/mapfiles/dir_" + (int) adjBearing + ".png");
            try {
                image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (image != null){

            // Anchor is ratio in range [0..1] so value of 0.5 on x and y will center the marker image on the lat/long
            float anchorX = 0.5f;
            float anchorY = 0.5f;

            int offsetX = 0;
            int offsetY = 0;

            // images are 24px x 24px
            // so transformed image will be 48px x 48px

            //315 range -- 22.5 either side of 315
            if (bearing >= 292.5 && bearing < 335.5){
                offsetX = 24;
                offsetY = 24;
            }
            //270 range
            else if (bearing >= 247.5 && bearing < 292.5){
                offsetX = 24;
                offsetY = 12;
            }
            //225 range
            else if (bearing >= 202.5 && bearing < 247.5){
                offsetX = 24;
                offsetY = 0;
            }
            //180 range
            else if (bearing >= 157.5 && bearing < 202.5){
                offsetX = 12;
                offsetY = 0;
            }
            //135 range
            else if (bearing >= 112.5 && bearing < 157.5){
                offsetX = 0;
                offsetY = 0;
            }
            //90 range
            else if (bearing >= 67.5 && bearing < 112.5){
                offsetX = 0;
                offsetY = 12;
            }
            //45 range
            else if (bearing >= 22.5 && bearing < 67.5){
                offsetX = 0;
                offsetY = 24;
            }
            //0 range - 335.5 - 22.5
            else {
                offsetX = 12;
                offsetY = 24;
            }

            Bitmap wideBmp;
            Canvas wideBmpCanvas;
            Rect src, dest;

            // Create larger bitmap 4 times the size of arrow head image
            wideBmp = Bitmap.createBitmap(image.getWidth() * 2, image.getHeight() * 2, image.getConfig());

            wideBmpCanvas = new Canvas(wideBmp);

            src = new Rect(0, 0, image.getWidth(), image.getHeight());
            dest = new Rect(src);
            dest.offset(offsetX, offsetY);

            wideBmpCanvas.drawBitmap(image, src, dest, null);

            mMap.addMarker(new MarkerOptions()
                    .position(to)
                    .icon(BitmapDescriptorFactory.fromBitmap(wideBmp))
                    .anchor(anchorX, anchorY));
        }
    }

    private double GetBearing(LatLng from, LatLng to){
        double lat1 = from.latitude * Math.PI / 180.0;
        double lon1 = from.longitude * Math.PI / 180.0;
        double lat2 = to.latitude * Math.PI / 180.0;
        double lon2 = to.longitude * Math.PI / 180.0;

        // Compute the angle.
        double angle = - Math.atan2( Math.sin( lon1 - lon2 ) * Math.cos( lat2 ), Math.cos( lat1 ) * Math.sin( lat2 ) - Math.sin( lat1 ) * Math.cos( lat2 ) * Math.cos( lon1 - lon2 ) );

        if (angle < 0.0)
            angle += Math.PI * 2.0;

        // And convert result to degrees.
        angle = angle * degreesPerRadian;

        return angle;
    }

    protected  void numberLiners() {



        final LinearLayout lm = findViewById(R.id.numliners);




        for ( int i = 0 ; i < vehicleNumber.length ; i++) {


            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);


            //      params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

            // create the layout params that will be used to define how your
            // button will be displayed

            LinearLayout lnrLay = new LinearLayout(this);
            //   Create LinearLayout

            RelativeLayout ll = new RelativeLayout(this);

            //  ll.setOrientation(RelativeLayout.HORIZONTAL);
            // Create TextView
            Button btn = new Button(this);
            // Give button an ID
            btn.setId(i);
            btn.setText(vehicleNumber[i]);
            //   btn.setWidth((lm.getWidth() * 35* 4) / 100);
            // set the layoutParams on the button
            btn.setLayoutParams(params);

            if(i != 0)
                btn.setBackgroundResource(R.drawable.roundedbutton1);
            else
                btn.setBackgroundResource(R.drawable.roundbuttongh);
            ((btn)).setTextColor(Color.parseColor("#ffffff"));
           // btn.setShadowLayer(3, 0, 0, Color.WHITE);
            //btn.setAlpha(0.3f);
            final int index = i;





            //Add button to LinearLayout defined in XML
            lm.addView(btn);


            btn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    final LinearLayout lm = findViewById(R.id.liners);
                    lm.removeAllViews();
                   // Log.e("vehicleList", "Cliecked on Vehicle Button ");
                    int inx = index;
                 //   ImeiNumber = IMEiArray [vehicleIndexSet];

                    mMap.clear();

                    poly.remove();
                    int preInd = vehicleIndexSet;
                    String URL = "hi";
                    vehicleIndexSet = v.getId();
                    currentIndexSet = 0;
                    Button  button = findViewById(preInd);// datGbV = v;
                    button.setBackgroundResource(R.drawable.roundedbutton1);
                    v.setBackgroundResource(R.drawable.roundbuttongh);
                    new DownloadXML1().execute(URL);


                }
            });

        }
    }


    protected void createLeftSlideDates() {


        final LinearLayout  lm =(LinearLayout) findViewById(R.id.liners);
        //lm.setAlpha(0.5f);
        lm.getBackground().setAlpha(50);








            for ( int i = RecDate.length-1 ; i >= 0 ; i--) {


                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);




              LinearLayout lnrLay = new LinearLayout(this);
                //   Create LinearLayout
                //
              RelativeLayout ll = new RelativeLayout(this);

                // ll.setOrientation(RelativeLayout.HORIZONTAL);
                // Create TextView
                Button btn = new Button(this);
                // Give button an ID
                btn.setId(i);

                String dt = RecDate[i] + "-" +MonthSet[Integer.parseInt(RecMonth[i])] + "-" +RecYear[i];//(RecDate[i] + "-" +RecMonth[i] + "-" +RecYear[i]);

                // set the layoutParams on the button
                btn.setText(dt);
            //  if(currentIndexSet == i)

                btn.setLayoutParams(params);
                if(RecDate.length-1 != i)
                btn.setBackgroundResource(R.drawable.roundedbutton1);
                else
                    btn.setBackgroundResource(R.drawable.roundbuttongh);

                ((btn)).setTextColor(Color.parseColor("#ffffff"));
               // btn.setShadowLayer(5, 0, 0, Color.WHITE);
                final int index = i;






                lm.addView(btn);

                btn.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {



//                        lm.removeAllViews();
                       final LinearLayout lm = (LinearLayout) findViewById(R.id.zone_list);
                        Log.e("lm", "onClick: "+lm );

                        mMap.clear();



                        int preInd = currentIndexSet;

                        currentIndexSet = v.getId();
           // Button  button = (Button) findViewById(preInd);// datGbV = v;
                        //SignInButton button = (SignInButton) findViewById(preInd);


                        // <-- object that belongs the view - the Button
                        Button button = ((Button)findViewById(preInd));
                        button.setBackgroundResource(R.drawable.roundedbutton1);




                       //View button = (View)v. findViewById(preInd);
                      //  button.setBackgroundResource(R.drawable.roundedbutton);

                        v.setBackgroundResource(R.drawable.roundbuttongh);

                            String URL = "hi";
                            new DownloadHisData().execute("0");






                    }

                });

            }


    }

    protected  void stUpMaps() {
        //      //   Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
            .findFragmentById(R.id.main_activity_map);


        //Let's calculate what is a radius in screen pixels for our icon
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        Drawable m_icon = ResourcesCompat.getDrawableForDensity(getResources(), R.drawable.truck, dm.densityDpi, null);
        int height = ((BitmapDrawable) m_icon).getBitmap().getHeight();
        int width = ((BitmapDrawable) m_icon).getBitmap().getWidth();

        iconRadiusPixels = width / 2;

        mapFragment.getMapAsync(this);
    }


}
