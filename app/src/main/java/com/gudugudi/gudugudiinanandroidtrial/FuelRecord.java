package com.gudugudi.gudugudiinanandroidtrial;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class FuelRecord extends AsyncTask<Void,Void,Void> {
  int allSets = 0;
  public String jsonFuelFileName = "";
  public String jsonFuelFileName1 = "";
  public JSONArray dateJsObjecth;
  public JSONArray hrJsObjecth;
  public JSONArray latiJsObjecth;
  public JSONArray longiJsObjecth;
  public JSONArray minJsObjecth;
  public JSONArray mnJsObjecth;
  public JSONArray secJsObjecth;
  public JSONArray spJsObjecth;
  public JSONArray yrJsObjecth;

  public JSONArray terJsObject;
  public JSONArray doorStJsObject;
  public JSONArray voltJsObject;
  public JSONArray hrJsObject;
  public JSONArray minJsObject;
  public JSONArray mnJsObject;
  public JSONArray secJsObject;
  public JSONArray yrJsObject;
  @Override
  protected void onProgressUpdate(Void... values) {
    super.onProgressUpdate(values);
  }
  @Override
  protected Void doInBackground(Void... voids) {
    try {

      //URL url=new URL("https://www.gudugudi.com/trackingnition/0353701091588554-logs/0353701091588554-1-11-18.json");

      Log.e("logs",jsonFuelFileName);
      URL url=new URL(jsonFuelFileName);
      URL url1=new URL("https://www.gudugudi.com/tracksensors/0351510090112551-logs/0351510090112551.json");
      // URL url=new URL("https://api.myjson.com/bins/y29mu");


      HttpURLConnection httpURLConnection =(HttpURLConnection)url.openConnection();
      HttpURLConnection httpURLConnection1 =(HttpURLConnection)url1.openConnection();
      InputStream inputStream=httpURLConnection.getInputStream();
      InputStream inputStream1=httpURLConnection1.getInputStream();
      Gson gson=new Gson();
      JsonElement element = new JsonParser().parse(new InputStreamReader(inputStream));
      JsonElement element1 = new JsonParser().parse(new InputStreamReader(inputStream1));
      JSONObject jsonObject = new JSONObject(element.getAsJsonObject().toString());
      JSONObject jsonObject1 = new JSONObject(element1.getAsJsonObject().toString());


      doorStJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("SensorStat").
          getJSONArray("doorStatus");
      hrJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("SensorStat").
          getJSONArray("hour");
      minJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("SensorStat").
          getJSONArray("minute");
      voltJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("SensorStat").
          getJSONArray("voltageLevel");
      mnJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("SensorStat").
          getJSONArray("month");
      secJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("SensorStat").
          getJSONArray("seconds");
      terJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("SensorStat").
          getJSONArray("terminalStatus");
     /* voltJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("SensorStat").
          getJSONArray("voltageLevel");*/
      yrJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("SensorStat").
          getJSONArray("year");

      dateJsObjecth = jsonObject1.getJSONObject("Shoolman").getJSONObject("Histrack").
          getJSONArray("date");
      hrJsObjecth = jsonObject1.getJSONObject("Shoolman").getJSONObject("Histrack").
          getJSONArray("hour");
      latiJsObjecth = jsonObject1.getJSONObject("Shoolman").getJSONObject("Histrack").
          getJSONArray("latatitude");
      longiJsObjecth = jsonObject1.getJSONObject("Shoolman").getJSONObject("Histrack").
          getJSONArray("longitude");
      minJsObjecth = jsonObject1.getJSONObject("Shoolman").getJSONObject("Histrack").
          getJSONArray("minute");
      mnJsObjecth = jsonObject1.getJSONObject("Shoolman").getJSONObject("Histrack").
          getJSONArray("month");
      secJsObjecth = jsonObject1.getJSONObject("Shoolman").getJSONObject("Histrack").
          getJSONArray("seconds");
      spJsObjecth = jsonObject1.getJSONObject("Shoolman").getJSONObject("Histrack").
          getJSONArray("speed");
      yrJsObjecth = jsonObject1.getJSONObject("Shoolman").getJSONObject("Histrack").
          getJSONArray("year");

      int i= 0;

      while( i < this.dateJsObjecth.length() ) {
        //Log.e("doorStJsObject", "******doorStJsObject*********" + this.doorStJsObject.get(i));
        Log.e("hrJsObject", "******Hour*********" + this.hrJsObject.get(i));
        Log.e("minJsObject", "******minJsObject*********" + this.minJsObject.get(i));
        Log.e("mnJsObject", "******month*********" + this.mnJsObject.get(i));
        Log.e("secJsObject", "******seconds*********" + this.secJsObject.get(i));
        //Log.e("terJsObject", "******terminalsat*********" + this.terJsObject.get(i));
        Log.e("voltJsObject", "******voltage2*********" + this.voltJsObject.get(i));
        Log.e("year", "******year*********" + this.yrJsObject.get(i));

        i++;
      }


      int j = 0;
      while( j < this.dateJsObjecth.length() ){
        Log.e("dateJsObjecth", "******dateh1*********" + this.dateJsObjecth.get(j));
        Log.e("voltJsObject", "******voltagej*********" + this.voltJsObject.get(j));
        /*Log.e("hrJsObjecth", "******hourh*********" + this.hrJsObjecth.get(j));
        Log.e("latiJsObjecth", "******latih*********" + this.latiJsObjecth.get(j));
        Log.e("longiJsObjecth", "******longih*********" + this.longiJsObjecth.get(j));
        Log.e("minJsObjecth", "******minh*********" + this.minJsObjecth.get(j));
        */
        Log.e("mnJsObjecth", "******monh*********" + this.mnJsObjecth.get(j));
        //Log.e("secJsObjecth", "******dateh*********" + this.secJsObjecth.get(j));
        Log.e("yearh", "******yearh*********" + this.yrJsObjecth.get(j));

        j++;
      }



      allSets = 1;
    } catch (MalformedURLException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    catch (JSONException e) {
      e.printStackTrace();
    }

    return null;
  }
  @Override
  protected void onPostExecute(Void aVoid) {
    super.onPostExecute(aVoid);
    // LoginActivity.data.setText(this.dataparsed);
  }
}
