package com.gudugudi.gudugudiinanandroidtrial;

import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;

import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.SphericalUtil;

import org.json.JSONException;

public class FuelReport extends MainActivity{

  public static TextView data;

  GoogleMap mMap;
  private LatLng sydney1;
  private LatLng BANGALORE;
  private int iconRadiusPixels;
  private FuelRecord jsonTrackRcrd;
  Polyline poly;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.fuel_list);
    FuelData jsnExtarct = new FuelData();
    jsnExtarct.execute();
     //jsnExtarct.getClass();
    try {


      while(jsnExtarct.allSets == 0){
        ;
      }


      createLeftSlideDates(jsnExtarct);

      fuelList(jsnExtarct);


      FuelRecord jsnTrckRecord = new FuelRecord();
      String filName = "https://www.gudugudi.com/tracksensors/0351510090112551-logs/0351510090112551";
      filName = filName + "-" + jsnExtarct.dateJsObjecth.get(jsnExtarct.dateJsObjecth.length()-1).toString()
          + "-" + jsnExtarct.mnJsObjecth.get(jsnExtarct.mnJsObjecth.length()-1).toString()
          + "-" + jsnExtarct.yrJsObjecth.get(jsnExtarct.yrJsObjecth.length()-1).toString()+ ".json";

      jsnTrckRecord.jsonFuelFileName = filName;

      jsnTrckRecord.execute();
      Log.e("dummy11", ""+ jsnTrckRecord.jsonFuelFileName);
      Log.e("filename", ""+filName);

      while(jsnTrckRecord.allSets == 0){
        ;
      }

      jsonTrackRcrd = jsnTrckRecord;



    }
    catch (JSONException e) {

    }
  }


  protected void fuelList(final FuelData jsnExtarct) {


    final LinearLayout lm = findViewById(R.id.zone_list);


    try {

      // params.addRule(RelativeLayout.BELOW);

      //ll.setOrientation(RelativeLayout.HORIZONTAL);

      // ll.setLayoutParams(params);




      for ( int i = jsnExtarct.dateJsObjecth.length() -1 ; i >= 0 ; i--) {


        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);


        //      params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

        // create the layout params that will be used to define how your
        // button will be displayed

        LinearLayout lnrLay = new LinearLayout(this);
        //   Create LinearLayout

        RelativeLayout ll = new RelativeLayout(this);

        // ll.setOrientation(RelativeLayout.HORIZONTAL);
        // Create TextView
        ListView btn = new ListView(this);
        // Give button an ID
        btn.setId(i);

        String[] status = {
            jsnExtarct.voltJsObject.get(i).toString()
        };
        btn.setAdapter(new ArrayAdapter<String>(FuelReport.this,
            android.R.layout.simple_list_item_1, status));
        // btn.setText(Integer.toString((Integer) jsnExtarct.alarmTypeJsObject.get(i)));
       // btn.setText(jsnExtarct.voltJsObject.get(i).toString());
        //btn.setWidth((lm.getWidth() * 35* 4) / 100);
        // set the layoutParams on the button
        btn.setLayoutParams(params);

        lm.addView(btn);

      }

    }
    catch (JSONException e) {

    }

  }



  protected void createLeftSlideDates(final FuelData jsnExtarct) {

    final LinearLayout lm = (LinearLayout) findViewById(R.id.liners);


    try {

      // params.addRule(RelativeLayout.BELOW);

      //ll.setOrientation(RelativeLayout.HORIZONTAL);

      // ll.setLayoutParams(params);




      for ( int i = jsnExtarct.dateJsObjecth.length() -1 ; i >= 0 ; i--) {


        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);


        //      params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

        // create the layout params that will be used to define how your
        // button will be displayed

        LinearLayout lnrLay = new LinearLayout(this);
        //   Create LinearLayout

        RelativeLayout ll = new RelativeLayout(this);

        // ll.setOrientation(RelativeLayout.HORIZONTAL);
        // Create TextView
        Button btn = new Button(this);
        // Give button an ID
        btn.setId(i);
        btn.setText(jsnExtarct.dateJsObjecth.get(i).toString() + "   " +
            jsnExtarct.mnJsObject.get(i).toString() + "   " + jsnExtarct.yrJsObject.get(i)
            .toString());
        btn.setWidth((lm.getWidth() * 35* 4) / 100);
        // set the layoutParams on the button
        btn.setLayoutParams(params);

        final int index = i;

        btn.setOnClickListener(new View.OnClickListener() {
          public void onClick(View v) {

            try {
              int inx = index;
              FuelRecord jsnTrckRecord = new FuelRecord();
              String filName = "https://www.gudugudi.com/tracksensors/0351510090112551-logs/0351510090112551";
              filName = filName + "-" + jsnExtarct.dateJsObjecth.get(inx).toString() + "-"
                  + jsnExtarct.mnJsObject.get(inx).toString()+ "-" +
                  jsnExtarct.yrJsObject.get(inx).toString()+ ".json";
              jsnTrckRecord.jsonFuelFileName = filName;

              jsnTrckRecord.execute();
              //   jsnExtarct.getJsonData();


              while(jsnTrckRecord.allSets == 0){
                ;
              }

              jsonTrackRcrd = jsnTrckRecord;

              Log.i("TAG", "Date :" + jsonTrackRcrd.dateJsObjecth.get(inx).toString()+
                  "Month :" + jsonTrackRcrd.mnJsObject.get(inx).toString()+
                  "Year :" + jsonTrackRcrd.yrJsObject.get(inx).toString());

              Toast.makeText(getApplicationContext(),
                  "Date :" + jsonTrackRcrd.dateJsObjecth.get(inx).toString()+
                      "Month :" + jsonTrackRcrd.mnJsObject.get(inx).toString()+
                      "Year :" + jsonTrackRcrd.yrJsObject.get(inx).toString(),
                  Toast.LENGTH_LONG).show();

            } catch (JSONException e) {
              ;
            }
          }

        });


//                Toast.makeText(getApplicationContext(),
//                        "Index:"+ i ,
//                        Toast.LENGTH_LONG).show();
        //Add button to LinearLayout

        //       lnrLay .addView(btn);
//                lnrLay.addView(ll);
        //Add button to LinearLayout defined in XML
        lm.addView(btn);

      }

    }
    catch (JSONException e) {

    }

  }
}
