
package com.gudugudi.gudugudiinanandroidtrial;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;

import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.SphericalUtil;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class MapActivity extends MainActivity  implements OnMapReadyCallback, GoogleMap.OnCameraMoveStartedListener, GoogleMap.OnCameraIdleListener {
//    Button click;
//    public static TextView data;
//    private GoogleMap mMap;
//    private LatLng sydney1;
//    private LatLng BANGALORE;
//    private int iconRadiusPixels;
//    Polyline poly;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
//        JsonData jsnExtarct = new JsonData();
//        jsnExtarct.execute();
//        //   jsnExtarct.getJsonData();
//
//
//        while(jsnExtarct.allSets == 0){
//            ;
//        }
//
//
//        createLeftSlideDates(jsnExtarct);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.map);
//
//
//        //Let's calculate what is a radius in screen pixels for our icon
//        DisplayMetrics dm = new DisplayMetrics();
//        getWindowManager().getDefaultDisplay().getMetrics(dm);
//        Drawable m_icon = ResourcesCompat.getDrawableForDensity(getResources(), R.drawable.truck, dm.densityDpi, null);
//        int height = ((BitmapDrawable) m_icon).getBitmap().getHeight();
//        int width = ((BitmapDrawable) m_icon).getBitmap().getWidth();
//
//        iconRadiusPixels = width / 2;
//
//        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
//        mMap = googleMap;
//
//        mMap.setOnCameraMoveStartedListener(this);
//        mMap.setOnCameraIdleListener(this);
//
//        mMap.getUiSettings().setZoomControlsEnabled(true);

//        // Add a marker in Sydney and move the camera
//        sydney1 = new LatLng(-33.904438, 151.249852);
//        BANGALORE = new LatLng(-12.934533 ,77.626579);
//
//        BitmapDescriptor iconDescr = BitmapDescriptorFactory.fromResource(R.drawable.truck);
//        /*BitmapDescriptor iconDescr = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE);*/
//        mMap.addMarker(new MarkerOptions().position(sydney1).anchor(0.5F, 0.5F)
//                .draggable(false).visible(true).title("Marker in Sydney 1")
//                .icon(iconDescr));
//        mMap.addMarker(new MarkerOptions().position(BANGALORE).anchor(0.5F, 0.5F)
//                .draggable(false).visible(true).title("Marker in Sydney 2")
//                .icon(iconDescr));
//
//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney1, 17F));
    }

    @Override
    public void onCameraMoveStarted(int reason) {

    }

    @Override
    public void onCameraIdle() {
//        if (this.poly != null) {
//            this.poly.remove();
//            this.poly = null;
//        }
//        this.showCustomPolyline();
    }



//
//
//
//    private void showCustomPolyline() {
////        //Get projection
////        Projection proj = mMap.getProjection();
////        //Get a point on the screen that corresponds to first marker
////        Point p = proj.toScreenLocation(sydney1);
////        //Lets create another point that is shifted on number of pixels iqual to icon radius
////        //This point is located on circle border
////        Point b = new Point(p.x + iconRadiusPixels, p.y);
////        //Get the LatLng for a point on the circle border
////        LatLng l = proj.fromScreenLocation(b);
////        //Calculate the radius of the icon (distance between center and point on the circle border)
////        double r = SphericalUtil.computeDistanceBetween(sydney1, l);
////        //Calculate heading from point 1 to point 2 and from point 2 to point 1
////        double heading1 = SphericalUtil.computeHeading(sydney1, BANGALORE);
////        double heading2 = SphericalUtil.computeHeading(BANGALORE, sydney1);
////
////        //Calculate real position where the polyline starts and ends taking into account radius and heading
////        LatLng pos1 = SphericalUtil.computeOffset(sydney1, r, heading1);
////        LatLng pos2 = SphericalUtil.computeOffset(BANGALORE, r, heading2);
////
////        //Create polyline
////        PolylineOptions options = new PolylineOptions();
////        options.add(pos1);
////        options.add(pos2);
////
////        poly = mMap.addPolyline(options.width(8).color(Color.RED).geodesic(false));
//    }
//
//    protected void createLeftSlideDates(final JsonData jsnExtarct) {
//
////        final LinearLayout lm = (LinearLayout) findViewById(R.id.liners);
////
////
////        // ll.setLayoutParams(params);
////
////        try {
////
////
////            for ( int i = jsnExtarct.dateJsObject.length() -1 ; i >= 0 ; i--) {
////
////
////                // create the layout params that will be used to define how your
////                // button will be displayed
////                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
////                        RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
////
////
////                params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
////
////                //   Create LinearLayout
////                RelativeLayout ll = new RelativeLayout(this);
////                //ll.setOrientation(RelativeLayout.HORIZONTAL);
////
////
////                // Create TextView
////                Button btn = new Button(this);
////                // Give button an ID
////                btn.setId(i);
////                btn.setText(jsnExtarct.dateJsObject.get(i).toString() + "   " +
////                        jsnExtarct.mnJsObject.get(i).toString() + "   " + jsnExtarct.yrJsObject.get(i)
////                        .toString());
////                btn.setWidth((lm.getWidth() * 35* 4) / 100);
////                // set the layoutParams on the button
////                btn.setLayoutParams(params);
////
////                final int index = i;
////
////                btn.setOnClickListener(new View.OnClickListener() {
////                    public void onClick(View v) {
////
////                        try {
////                            int inx = index;
////
////                            Log.i("TAG", "Date :" + jsnExtarct.dateJsObject.get(inx).toString()+
////                                    "Month :" + jsnExtarct.dateJsObject.get(inx).toString()+
////                                    "Year :" + jsnExtarct.dateJsObject.get(inx).toString());
////
////                            Toast.makeText(getApplicationContext(),
////                                    "Date :" + jsnExtarct.dateJsObject.get(inx).toString()+
////                                            "Month :" + jsnExtarct.mnJsObject.get(inx).toString()+
////                                            "Year :" + jsnExtarct.yrJsObject.get(inx).toString(),
////                                    Toast.LENGTH_LONG).show();
////
////                        } catch (JSONException e) {
////                            ;
////                        }
////                    }
////
////                });
////
////                //Add button to LinearLayout
////                ll.addView(btn);
////                //Add button to LinearLayout defined in XML
////                lm.addView(ll);
////
////            }
////
////        }
////        catch (JSONException e) {
////
////        }
//
//    }

}
