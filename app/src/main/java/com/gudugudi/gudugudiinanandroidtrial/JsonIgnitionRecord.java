package com.gudugudi.gudugudiinanandroidtrial;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class JsonIgnitionRecord  extends AsyncTask<Void,Void,Void> {
    String data="";
    int allSets = 0;
    public String jsonIgnitionFileName = "";
    public String jsonIgnitionFileName1 = "";
    public JSONArray dateJsObject;
    public JSONArray latiJsObject;
    public JSONArray longiJsObject;
    public JSONArray hrJsObject;
    public JSONArray ignitionJsObject;
    public JSONArray minJsObject;
    public JSONArray mnJsObject;
    public JSONArray secJsObject;
    public JSONArray yrJsObject;
    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }
    @Override
    protected Void doInBackground(Void... voids) {
        try {

            //URL url=new URL("https://www.gudugudi.com/trackingnition/0353701091588554-logs/0353701091588554-1-11-18.json");

           Log.e("logs",jsonIgnitionFileName);
           URL url=new URL(jsonIgnitionFileName);

            // URL url=new URL("https://api.myjson.com/bins/y29mu");


            HttpURLConnection httpURLConnection =(HttpURLConnection)url.openConnection();

            InputStream inputStream=httpURLConnection.getInputStream();

            Gson gson=new Gson();
            JsonElement element = new JsonParser().parse(new InputStreamReader(inputStream));

            JSONObject jsonObject = new JSONObject(element.getAsJsonObject().toString());

//          dateJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("Histrack").
//              getJSONArray("date");
//          latiJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("Histrack").
//              getJSONArray("latatitude");
//          longiJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("Histrack").
//              getJSONArray("longitude");
            hrJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("IngniStat").
                    getJSONArray("hour");
            ignitionJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("IngniStat").
                    getJSONArray("ingnition");
            minJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("IngniStat").
                    getJSONArray("minute");
            mnJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("IngniStat").
                    getJSONArray("month");
            secJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("IngniStat").
                    getJSONArray("seconds");
//            yrJsObject = jsonObject.getJSONObject("Shoolman").getJSONObject("IngniStat").
//                    getJSONArray("year");
//          int j = 0;
//          while( j < this.dateJsObject.length() ){
//            Log.e("logsjsonignidatadate", "******date*********" + this.dateJsObject.get(j));
//
//            j++;
//          }

            int i = 0;
//            while( i < this.dateJsObject.length() ) {
//
//                Log.e("logs", "******Hour*********" + this.hrJsObject.get(i));
//                Log.e("logs", "******Ignition*********" + this.ignitionJsObject.get(i));
//                Log.e("logs", "******Minute*********" + this.minJsObject.get(i));
//                Log.e("logs", "******Month*********" + this.mnJsObject.get(i));
//                Log.e("logs", "******Seconds*********" + this.secJsObject.get(i));
//                Log.e("logs", "******Year*********" + this.yrJsObject.get(i));
//                i++;
//            }

            Log.e("logs", "******AFter redainf******"+ allSets);
            allSets = 1;
            Log.e("logs", "******AFter redainf******"+ allSets);
        } catch (MalformedURLException e) {
            allSets = 1;
            e.printStackTrace();
        } catch (IOException e) {
            allSets = 1;
            e.printStackTrace();
        }
        catch (JSONException e) {
            allSets = 1;
            e.printStackTrace();
        }

        allSets = 1;
        return null;
    }
    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
       // LoginActivity.data.setText(this.dataparsed);
    }
}
