package com.gudugudi.gudugudiinanandroidtrial;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.content.res.ResourcesCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.SphericalUtil;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Locale;

public class TrackRecordReport extends MainActivity  {
    public static TextView data;

    GoogleMap mMap;
    private LatLng sydney1;
    private LatLng BANGALORE;
    private int iconRadiusPixels;
    int threadReles = 0;
    private  JsonTrackRecord jsonTrackRcrd;
    int numberVchicles;
    String ImeiNumber;
    Polyline poly;
    String IMEiArray[];
    String vehicleNumber[];
    String RecDate[];
    String RecMonth[];
    String RecYear[];
    String Latitude[];
    String Lanitude[];
    String Speed[];
    String DHour[];
    String DMinute[];
    String DSecond[];
    int currentIndexSet;
    int vehicleIndexSet;
    View datGbV;
    ProgressBar spinner;
    int firstTymDateClick;
    String MonthSet[] = {"jan","Jan","Feb","Mar","Apr","May","Jun",
            "Jul","Aug","Sep","Oct","Nov","Dec"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trackreport_list);


        IMEiArray = getIntent().getStringArrayExtra("IMEINumberArray");
        vehicleNumber = getIntent().getStringArrayExtra("VehicleNumbers");
        vehicleIndexSet = 0;
        currentIndexSet = 0;
        firstTymDateClick =0;
        String URL = "hi";
        new DownloadXML1().execute(URL);

        numberLiners();
    }



    private class DownloadXML1 extends AsyncTask<String, Void, Void> {

        private Object view;

        @Override

        protected Void doInBackground(String... Url) {



            int arrayIndex = 0;

            String connstr = " https://www.gudugudi.com/and_getHisDates.php";
            String result = "";
            int i = 0;
            try {

                result = "";
                URL url = new URL(connstr);
                HttpURLConnection http = (HttpURLConnection) url.openConnection();
                http.setRequestMethod("POST");
                http.setDoInput(true);
                http.setDoOutput(true);

                OutputStream ops = http.getOutputStream();

                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(ops, "UTF-8"));
                String data = URLEncoder.encode("imei", "UTF-8") + "=" + URLEncoder.encode(IMEiArray[vehicleIndexSet], "UTF-8");
                writer.write(data);
                writer.flush();
                writer.close();
                ops.close();

                InputStream ips = http.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(ips, "ISO-8859-1"));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    result += line;
                }
                if(result.length() < 1){
                    return  null;
                }
                Log.d("her gos", result);
                String[] strings = result.split(";");
                RecYear = strings[0].split(",");
                RecMonth = strings[1].split(",");
                RecDate = strings[2].split(",");



                Log.d("her gos", RecDate[0]);



            } catch (MalformedURLException e) {
                result = e.getMessage();
            } catch (IOException e) {
                result = e.getMessage();
            }
            return null;

        }


        protected void onPostExecute(Void args) {

            Log.d("After execution", RecDate[0]);
            currentIndexSet = RecDate.length-1;
            createLeftSlideDates();

            firstTymDateClick =0;
            String URL = "hi";
            new DownloadHisData().execute("0");

        }
    }
    private class DownloadHisData extends AsyncTask<String, Void, Void> {

        private Object view;

        @Override

        protected Void doInBackground(String... Url) {




            int arrayIndex = 0;
            //get number feild
            // for (int i = 0; i < trucKame.getLength(); i++) {
            // for (int i = 0; i < VechicleNumbers.length; i++) {

            String connstr = " https://www.gudugudi.com/and_getTrckRecordByImei.php";
            String result = "";
            int i = 0;
            try {

                result = "";
                URL url = new URL(connstr);
                HttpURLConnection http = (HttpURLConnection) url.openConnection();
                http.setRequestMethod("POST");
                http.setDoInput(true);
                http.setDoOutput(true);

                OutputStream ops = http.getOutputStream();

                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(ops, "UTF-8"));
                String data = URLEncoder.encode("imei", "UTF-8") + "=" + URLEncoder.encode(IMEiArray[vehicleIndexSet], "UTF-8")
                        +"&&"+URLEncoder.encode("date","UTF-8")+"="+URLEncoder.encode(RecDate[currentIndexSet],"UTF-8")
                        +"&&"+URLEncoder.encode("month","UTF-8")+"="+URLEncoder.encode(RecMonth[currentIndexSet],"UTF-8")
                        +"&&"+URLEncoder.encode("year","UTF-8")+"="+URLEncoder.encode(RecYear[currentIndexSet],"UTF-8");
                writer.write(data);
                writer.flush();
                writer.close();
                ops.close();

                InputStream ips = http.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(ips, "ISO-8859-1"));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    result += line;
                }
                if(result.length() < 1){
                    return  null;
                }
                Log.d("Now Histrack data", result);
                String[] strings = result.split(";");
                Latitude = strings[0].split(",");
                Lanitude= strings[1].split(",");
                Speed = strings[2].split(",");
                DHour = strings[3].split(",");
                DMinute = strings[4].split(",");
                DSecond = strings[5].split(",");
                // Log.d("her gos", RecDate[0]);



            } catch (MalformedURLException e) {
                result = e.getMessage();
            } catch (IOException e) {
                result = e.getMessage();
            }
            return null;

        }


        protected void onPostExecute(Void args) {

          trackrecorlist();

        }
    }

    protected void trackrecorlist() {
        String address;// = addresses.get(0).getAddressLine(0);
        String city; //= addresses.get(0).getAddressLine(1);
        String country; //= addresses.get(0).getAddressLine(2);


        final LinearLayout lm = (LinearLayout) findViewById(R.id.zone_list);



        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);




        LinearLayout lnrLay = new LinearLayout(this);
        //   Create LinearLayout

        RelativeLayout ll = new RelativeLayout(this);



        // ll.setOrientation(RelativeLayout.HORIZONTAL);
        // Create TextView
        Button btn = new Button(this);
        // Give button an ID
        btn.setId(0);
        Button btns = (Button) findViewById(R.id.head);

        btns.setText( "Speed" + " | " + " Location" + " | " + "Time" );
        // btn.setWidth((lm.getWidth() * 35* 4) / 100);
        // set the layoutParams on the button
        //params.setMargins(60, 0, 0, 0);
        // btn.setLayoutParams(params);
        Display display = ((WindowManager)getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();
        btn.setLayoutParams(new LinearLayout.LayoutParams((int) (width/(1.35)), height/10));
        Log.e("(width/(1.5))", "ignitionList: "+(int)(width/(1.35)) );
        btn.setLayoutParams(params);
        btn.setBackgroundResource(R.drawable.roundedbutton1);
        ((btn)).setTextColor(Color.parseColor("#ffffff"));
        lm.addView(btn);

        int hrsStrt, minStrt , secStrt;
       // String igState = IgnitnState[0].toString();

        spinner = (ProgressBar)findViewById(R.id.progressBar);


        spinner.setVisibility(View.VISIBLE);


        for ( int i = 0 ; i < Latitude.length  ; i++) {


            LinearLayout.LayoutParams paramsa = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);

            Geocoder geoCoder = new Geocoder(getApplicationContext(),
                    Locale.getDefault());
            // while(jsonTrackRcrd.allSets == 0){
            // ;
            // Log.e("latitude&longi", "onMapReady: "+latList[j] +" ." + langList[j]);
            //}
            try {

            List<Address> addresses;


            addresses = geoCoder.getFromLocation(Double.parseDouble(Latitude[i].toString()),
                    Double.parseDouble(Lanitude[i].toString()), 1);

            address = addresses.get(0).getAddressLine(0);
            city = addresses.get(0).getAddressLine(1);




                Button btna = new Button(this);
                // Give button an ID
                btna.setId(i);




                    btna.setText("Speed-->" + Speed[i].toString() + "\n" +
                           "Loaction--->" + address + " " + city + " " + "\n" +
                            "Time------> "+ DHour[i].toString() + " Hr  " +
                    DMinute[i].toString() + " Mins " + DSecond[i].toString() + " Secs ");

                lm.addView(btna);
                btna.setBackgroundResource(R.drawable.roundedbutton1);
                ((btna)).setTextColor(Color.parseColor("#ffffff"));
                //paramsa.setMargins(50, 0, 0, 0);
                btna.setLayoutParams(paramsa);

            }catch (IOException e) {
                e.printStackTrace();
            }






        }



      spinner.setVisibility(View.GONE);









    }

    public double totalDistanceTravalled() {

        int i = 0;
        double distance = 0 ;
        while( i < Latitude .length -2) {

            distance =  distance + setDistance(i);
            i++;
        }

        return distance;

    }
    public double setDistance(int i) {

        return  lngDistance(Double.parseDouble(Latitude [i].toString()),Double.parseDouble(Lanitude [i].toString())
                , Double.parseDouble(Latitude [i + 1].toString()),
                Double.parseDouble(Lanitude [i + 1].toString()) );
    }
    public double lngDistance(double lat1,double lon1,double lat2,double lon2) {
        int R = 6371; // km (change this constant to get miles)
        double dLat = (lat2-lat1) * Math.PI / 180;
        double dLon = (lon2-lon1) * Math.PI / 180;
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(lat1 * Math.PI / 180 ) * Math.cos(lat2 * Math.PI / 180 ) *
                        Math.sin(dLon/2) * Math.sin(dLon/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double d = R * c;
    /*	if (d>1) {
               return d; //+"km";
         }
    	else if (d<=1) {
           return d;/// +"m";
       } */
        return d;
    }





    private final double degreesPerRadian = 180.0 / Math.PI;



    private double GetBearing(LatLng from, LatLng to){
        double lat1 = from.latitude * Math.PI / 180.0;
        double lon1 = from.longitude * Math.PI / 180.0;
        double lat2 = to.latitude * Math.PI / 180.0;
        double lon2 = to.longitude * Math.PI / 180.0;

        // Compute the angle.
        double angle = - Math.atan2( Math.sin( lon1 - lon2 ) * Math.cos( lat2 ), Math.cos( lat1 ) * Math.sin( lat2 ) - Math.sin( lat1 ) * Math.cos( lat2 ) * Math.cos( lon1 - lon2 ) );

        if (angle < 0.0)
            angle += Math.PI * 2.0;

        // And convert result to degrees.
        angle = angle * degreesPerRadian;

        return angle;
    }

    protected  void numberLiners() {



        final LinearLayout lm = (LinearLayout) findViewById(R.id.numliners);




        for ( int i = 0 ; i < vehicleNumber.length ; i++) {


            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);


            //      params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

            // create the layout params that will be used to define how your
            // button will be displayed

            LinearLayout lnrLay = new LinearLayout(this);
            //   Create LinearLayout

            RelativeLayout ll = new RelativeLayout(this);

            //  ll.setOrientation(RelativeLayout.HORIZONTAL);
            // Create TextView
            Button btn = new Button(this);
            // Give button an ID
            btn.setId(i);
            btn.setText(vehicleNumber[i]);
            //   btn.setWidth((lm.getWidth() * 35* 4) / 100);
            // set the layoutParams on the button
            btn.setLayoutParams(params);

            if(i != 0)
                btn.setBackgroundResource(R.drawable.roundedbutton1);
            else
                btn.setBackgroundResource(R.drawable.roundbuttongh);
            ((btn)).setTextColor(Color.parseColor("#ffffff"));
            // btn.setShadowLayer(3, 0, 0, Color.WHITE);
            //btn.setAlpha(0.3f);
            final int index = i;





            //Add button to LinearLayout defined in XML
            lm.addView(btn);


            btn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    final LinearLayout lm = (LinearLayout) findViewById(R.id.liners);
                    lm.removeAllViews();
                    // Log.e("vehicleList", "Cliecked on Vehicle Button ");
                    int inx = index;
                    //   ImeiNumber = IMEiArray [vehicleIndexSet];

                   // mMap.clear();

                //    poly.remove();
                    int preInd = vehicleIndexSet;
                    String URL = "hi";
                    vehicleIndexSet = v.getId();
                    currentIndexSet = 0;
                    Button  button = (Button) findViewById(preInd);// datGbV = v;
                    button.setBackgroundResource(R.drawable.roundedbutton1);
                    v.setBackgroundResource(R.drawable.roundbuttongh);
                    new DownloadXML1().execute(URL);


                }
            });

        }
    }


    protected void createLeftSlideDates() {

        final LinearLayout lm = (LinearLayout) findViewById(R.id.liners);
        //lm.setAlpha(0.5f);
        lm.getBackground().setAlpha(50);








        for ( int i = RecDate.length-1 ; i >= 0 ; i--) {


            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);




            LinearLayout lnrLay = new LinearLayout(this);
            //   Create LinearLayout

            RelativeLayout ll = new RelativeLayout(this);

            // ll.setOrientation(RelativeLayout.HORIZONTAL);
            // Create TextView
            Button btn = new Button(this);
            // Give button an ID
            btn.setId(i);

            String dt = RecDate[i] + "-" +MonthSet[Integer.parseInt(RecMonth[i].toString())] + "-" +RecYear[i];//(RecDate[i] + "-" +RecMonth[i] + "-" +RecYear[i]);

            // set the layoutParams on the button
            btn.setText(dt);
            //  if(currentIndexSet == i)

            btn.setLayoutParams(params);
            if(RecDate.length-1 != i)
                btn.setBackgroundResource(R.drawable.roundedbutton1);
            else
                btn.setBackgroundResource(R.drawable.roundbuttongh);

            ((btn)).setTextColor(Color.parseColor("#ffffff"));
            // btn.setShadowLayer(5, 0, 0, Color.WHITE);
            final int index = i;



            lm.addView(btn);

            btn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    // mMap.clear();
                    final LinearLayout lm = (LinearLayout) findViewById(R.id.zone_list);

                    lm.removeAllViews();
                    int preInd = currentIndexSet;


                    currentIndexSet = v.getId();

                    spinner.setVisibility(View.VISIBLE);

                    // datGbV.setBackgroundResource(R.drawable.roundedbutton1);
                    Button button = (Button) findViewById(preInd);// datGbV = v;
                    button.setBackgroundResource(R.drawable.roundedbutton1);

                    v.setBackgroundResource(R.drawable.roundbuttongh);

                    String URL = "hi";
                    new DownloadHisData().execute("0");



                }

            });

        }


    }



}
